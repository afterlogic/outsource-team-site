FROM node:8-alpine

ARG APP_DIR=app
RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}

RUN npm install -g gulp

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 9000

CMD ["gulp", "watch"]