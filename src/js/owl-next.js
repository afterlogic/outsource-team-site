$(document).ready(function() {
  $('.cases__item').each(function() {
    var element = this;
    new ScrollMagic.Scene({
      duration: 0,
      triggerElement: element,
      duration: 0,
      triggerHook: 0
    })
      .on('enter', function() {
        $(element)
          .find('.owl-next')
          .addClass('active');
      })
      .on('leave', function() {
        $(element)
          .find('.owl-next')
          .removeClass('active');
      })
      .addTo(window.SM.oMainController);
  });
});
