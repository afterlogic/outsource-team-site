! function (e) {
  function a(e, a) {
    var s = new RegExp("(^|\\s)" + a + "(\\s|$)", "g");
    s.test(e.className) || (e.className = (e.className + " " + a).replace(/\s+/g, " ").replace(/(^ | $)/g, ""))
  }

  function s(e, a) {
    var s = new RegExp("(^|\\s)" + a + "(\\s|$)", "g");
    e.className = e.className.replace(s, "$1").replace(/\s+/g, " ").replace(/(^ | $)/g, "")
  }

  function t(e, a) {
    var s = new RegExp("(^|\\s)" + a + "(\\s|$)", "g");
    return s.test(e.className) ? !0 : !1
  }
  var r = document.querySelectorAll.bind(document);
  e.pureTabs = {
    toggle: function (e) {
      var t = this.activeClassName,
        c = r("[data-puretabs]")[0],
        l = r(e.currentTarget.hash)[0];
      s(r(t)[0], t.substr(1)), a(e.currentTarget, t.substr(1)), c.classList.remove('active'), c.removeAttribute("data-puretabs"), l.classList.add('active'), l.setAttribute("data-puretabs", "")
    },
    init: function (e, a) {
      var s = this;
      s.className = "." + e || ".puretabs", s.activeClassName = "." + a || ".puretabs--active";
      var c = [].slice.call(r(s.className));
      c.forEach(function (e) {
        t(e, s.activeClassName.substr(1)) ? r(e.hash)[0].setAttribute("data-puretabs", "") : r(e.hash)[0].classList.remove('active'), e.addEventListener("click", function (e) {
          e.preventDefault(), s.toggle.call(s, e)
        })
      })
    }
  }
}(window);