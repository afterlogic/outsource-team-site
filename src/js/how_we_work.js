$(document).ready(function() {
	//how_we_work slide animation
	var 
		aTweens = null,
		iTriggerHook = 0.5
	;
	
	if (window.matchMedia('all and (min-width: 1280px)').matches)
	{
		aTweens = [
			TweenMax.from(".how_we_work .stage", 1, {opacity: 0, ease: Power2.easeOut}),
			TweenMax.from(".how_we_work .stage1", 1, {left: '-5%', ease: Power2.easeOut}),
			TweenMax.from(".how_we_work .stage2", 1, {left: '-20%', ease: Power2.easeOut}),
			TweenMax.from(".how_we_work .stage3", 1, {left: '-35%', ease: Power2.easeOut}),
			TweenMax.from(".how_we_work .stage4", 1, {left: '-50%', ease: Power2.easeOut}),
			TweenMax.from(".how_we_work .stage5", 1, {left: '-65%', ease: Power2.easeOut}),
			TweenMax.from(".how_we_work .stage6", 1, {left: '-80%', ease: Power2.easeOut})
		];
	}
	else
	{
		iTriggerHook = 0.3;
		
		aTweens = [
			TweenMax.from(".how_we_work .stage1", 1, {top: '-50px', ease: Power2.easeOut}),
			TweenMax.from(".how_we_work .stage2", 1, {top: '-220px', ease: Power2.easeOut}),
			TweenMax.from(".how_we_work .stage3", 1, {top: '-390px', ease: Power2.easeOut}),
			TweenMax.from(".how_we_work .stage4", 1, {top: '-560px', ease: Power2.easeOut}),
			TweenMax.from(".how_we_work .stage5", 1, {top: '-730px', ease: Power2.easeOut}),
			TweenMax.from(".how_we_work .stage6", 1, {top: '-900px', ease: Power2.easeOut})
		];
	}
	
	new ScrollMagic.Scene({
			duration: 0,
			triggerElement: "section.how_we_work",
			triggerHook: iTriggerHook
		})
		.setTween(new TimelineMax({
			tweens: aTweens,
			align: 'start'
		}))
		.addTo(window.SM.oMainController);
	
	
	//animation of texts
	new ScrollMagic.Scene({
		duration: 0,
		triggerElement: "section.how_we_work",
		triggerHook: 0.6
	})
	.setTween(new TimelineMax({
		tweens: [
			TweenMax.staggerFrom("section.how_we_work h2, section.how_we_work .description", 1, {opacity: 0, bottom: window.app.sTextAppearanceDistance, ease: Power2.easeOut}, 0.5)
		],
		align: 'start'
	}))
	.addTo(window.SM.oMainController);
});
