window.SM = {
	'oMainController': new ScrollMagic.Controller({
//		addIndicators: true
	})
};


$(document).ready(function() {
    console.log('start animation');
	$('section:not(.splash)').addClass('hiddenSection');

	var
		oScene = null,
		iDuration = 1,
		oAutoAnimTimeline = new TimelineMax({
			repeat: 0,
			delay: 1.4,
			tweens: [
				TweenMax.from("#cover", iDuration, {height: '40%', ease: Power3.easeOut}),
				TweenMax.from("#el1", iDuration, {bottom: '40%', ease: Power3.easeOut}),
				TweenMax.from("#el3", iDuration, {bottom: '40%', ease: Power3.easeOut}),

				TweenMax.from("#el2", iDuration, {bottom: '20%', ease: Power3.easeOut}),
				TweenMax.from("#el4", iDuration, {bottom: '20%', ease: Power3.easeOut}),

				TweenMax.from("#el5", iDuration, {bottom: '15%', ease: Power3.easeOut}),
				TweenMax.from("#el6", iDuration, {bottom: '15%', ease: Power3.easeOut}),
				TweenMax.from("#mask", iDuration, {opacity: 1, ease: Power3.easeOut})
			],
			onComplete: function () {
				oAutoAnimTimeline.clear();
				$('#mask').css({'opacity': 0});
				$('section:not(.splash)').removeClass('hiddenSection');
				
				var oElem = $("section.splash .wrapper");
				oElem.css({'top': oElem.offset()['top'], 'left': 0, 'position': 'fixed'});
				
				if (!oScene) {
					var oScrollAnimTimeline = new TimelineMax();
					
					oScrollAnimTimeline
						.add(TweenMax.to("section.splash .mouse", 0.3, { opacity: 0, ease: Linear.easeNone}))
						.add(TweenMax.to("section.splash .description", 0.3, { opacity: 0, ease: Linear.easeNone}))
						.add(TweenMax.to("section.splash .title", 0.3, { opacity: 0, ease: Linear.easeNone}))
						.add(TweenMax.to("section.splash .navbar", 0.3, {opacity: 0, ease: Linear.easeNone}))
						.add(TweenMax.to("section.splash .main_logo", 0.3, {opacity: 0, ease: Linear.easeNone}))
					;
					
					oScene = new ScrollMagic.Scene({
							duration: 300,
							offset: 0
						})
						.setTween(oScrollAnimTimeline)
//						.triggerHook(0)
//						.setPin("section.splash") // pins the element for the the scene's duration
						.addTo(window.SM.oMainController); // assign the scene to the controller
				
					new ScrollMagic.Scene({
							duration: 700,
							offset: 0
						})
						.setTween(new TimelineMax({
							tweens: [
								TweenMax.to("#cover", 2, {height: '10%', ease: Linear.easeNone}),
								TweenMax.to("#el1", 2, {bottom: '10%', ease: Linear.easeNone}),
								TweenMax.to("#el3", 2, {bottom: '10%', ease: Linear.easeNone}),

								TweenMax.to("#el2", 2, {bottom: '-30%', ease: Linear.easeNone}),
								TweenMax.to("#el4", 2, {bottom: '-30%', ease: Linear.easeNone}),

								TweenMax.to("#el5", 2, {bottom: '-70%', ease: Linear.easeNone}),
								TweenMax.to("#el6", 2, {bottom: '-70%', ease: Linear.easeNone}),

								TweenMax.to("#mask", 2, {opacity: 1, ease: Linear.easeNone})
								
//								TweenMax.to("section.splash .wrapper", 0.7, { opacity: 0, ease: Linear.easeNone})
							],
							align: 'start'
						}))
//						.triggerHook(0)
//						.setPin("section.splash") // pins the element for the the scene's duration
						.addTo(window.SM.oMainController); // assign the scene to the controller
				}
				
				$('.slider').data('owlCarousel').reinit();
				// $('.what_we_do__slider').data('owlCarousel').reinit();
			}
		})
	;

	new TweenMax.fromTo(".splash .mouse div", 1, {opacity: 0}, {repeat: -1, delay: 3, opacity: 1, yoyo: true, ease: Linear.easeNone});
	new TweenMax.from(".splash .title .cursor", 0.5, {opacity: 0, repeat: -1, ease: Cubic.easeInOut});
	new TweenMax.to(".splash .title .text", 1, {text:{value:"We do code", delimiter:""}, delay: 0.5, ease: Linear.easeNone});
        new TweenMax.to(".splash .errortitle .text", 1, {text:{value:"404", delimiter:""}, delay: 0.5, ease: Linear.easeNone});
	new TweenMax.from(".splash .description", 0.7, {opacity: 0, delay: 2, ease: Cubic.easeOut});
	new TweenMax.from(".splash .main_logo", 1, {opacity: 0, delay: 2.2, ease: Cubic.easeOut});
	new TweenMax.from(".splash .navbar", 1, {opacity: 0, delay: 2.4, ease: Cubic.easeOut});
});
