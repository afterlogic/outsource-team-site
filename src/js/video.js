$(document).ready(function() {
//	var
//		oControllerV = new ScrollMagic.Controller({
//			addIndicators: true
//		})
//	;
//
//	new ScrollMagic.Scene({
//			duration: 0,
//			triggerElement: "section.xura"
//		})
//		.triggerHook(1)
//		.addTo(oControllerV)
//		.on("enter", function (event) {
//			$('section.xura video')[0].play();
//		})
//		.on("leave", function (event) {
//			$('section.xura video')[0].pause();
//		})
//	;


	///// xura
	new ScrollMagic.Scene({
			duration: 0,
			triggerElement: "section.xura",
			triggerHook: 0.5
		})
		.setTween(TweenMax.from("section.xura video", 2, {opacity: 0, ease: Power2.easeOut}))
//		.addIndicators({name: "video xura"})
		.addTo(window.SM.oMainController);

	///// MEDICITY
	new ScrollMagic.Scene({
			duration: 0,
			triggerElement: "section.medicity",
			triggerHook: 0.5
		})
		.setTween(new TimelineMax({
			tweens: [
				TweenMax.from("section.medicity video", 2, {opacity: 0, ease: Power2.easeOut}),
				TweenMax.to("section.xura video", 2, {opacity: 0, ease: Power2.easeOut})
			],
			align: 'start'
		}))
//		.addIndicators({name: "video medicity"})
		.addTo(window.SM.oMainController);


	///// citybooq
	new ScrollMagic.Scene({
			duration: 0,
			triggerElement: "section.citybooq",
			triggerHook: 0.5
		})
		.setTween(new TimelineMax({
			tweens: [
				TweenMax.from("section.citybooq video", 2, {opacity: 0, ease: Power2.easeOut}),
				TweenMax.to("section.medicity video", 2, {opacity: 0, ease: Power2.easeOut})
			],
			align: 'start'
		}))
//		.addIndicators({name: "video citybooq"})
		.addTo(window.SM.oMainController);

	///// netvision
	new ScrollMagic.Scene({
			duration: 0,
			triggerElement: "section.netvision",
			triggerHook: 0.5
		})
		.setTween(new TimelineMax({
			tweens: [
				TweenMax.from("section.netvision video", 2, {opacity: 0, ease: Power2.easeOut}),
				TweenMax.to("section.citybooq video", 2, {opacity: 0, ease: Power2.easeOut})
			],
			align: 'start'
		}))
//		.addIndicators({name: "video netvision"})
		.addTo(window.SM.oMainController);
});
