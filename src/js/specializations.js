$(document).ready(function() {
  var specHeight;
  setTimeout(function() {
    specHeight = $('.our-specializations').height();

    new ScrollMagic.Scene({
      triggerElement: 'section.our-specializations',
      triggerHook: 0.1,
      duration: specHeight
    })
      .setClassToggle('.menu__btn', 'black')
      .addTo(window.SM.oMainController);
  }, 3700);

  $('.our-specializations__item').each(function() {
    new ScrollMagic.Scene({
      duration: 0,
      triggerElement: this,
      triggerHook: 0.8
    })
      .setTween(
        new TimelineMax({
          tweens: [
            TweenMax.from($(this), 0.8, {
              opacity: 0,
              y: 50,
              ease: Power2.easeOut
            })
          ]
        })
      )
      .addTo(window.SM.oMainController);
  });
});
