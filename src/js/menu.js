var menu = document.querySelector('.menu');
var menuBtn = document.querySelector('.menu__btn');
var overlay = document.querySelector('.menu__overlay');

menuBtn.addEventListener('click', menuToggle);

document.addEventListener('click', closeMenu);

function closeMenu(e) {
  if (!e.target.closest('.menu') && !Object.is(e.target, menuBtn)) {
    menu.classList.remove('menu_opened');
    menuBtn.classList.remove('active');
    overlay.classList.remove('active');
  }
}

function closeAfterScroll() {
  menu.classList.remove('menu_opened');
  menuBtn.classList.remove('active');
  overlay.classList.remove('active');
}

function menuToggle() {
  var menuScene = new ScrollMagic.Scene({
    triggerElement: 'section.what_we_do',
    duration: 0
  }).setClassToggle('.menu', 'menu_opened');

  menuScene.on('leave', function(event) {
    menuScene.destroy(true);
    menuBtn.classList.remove('active');
    overlay.classList.remove('active');
  });

  var classStatus = menu.classList.contains('menu_opened');
  if (classStatus) {
    menu.classList.remove('menu_opened');
    menuBtn.classList.remove('active');
    overlay.classList.remove('active');
    window.SM.oMainController.removeScene(menuScene);
  } else {
    menu.classList.add('menu_opened');
    menuBtn.classList.add('active');
    overlay.classList.add('active');
    window.SM.oMainController.addScene(menuScene);
  }
}

var spy = new Gumshoe('#menu a');
var scroll = new SmoothScroll('a[href*="#"]', {
  speed: 250
});

$(document).ready(function() {
  new ScrollMagic.Scene({
    offset: 0,
    triggerElement: 'section.how_we_work',
    triggerHook: 0.3
  })
    .setClassToggle('.menu__btn', 'black')
    .addTo(window.SM.oMainController);
});

document.addEventListener('scrollStop', closeAfterScroll);
