$(document).ready(function () {

	var className = 'what_we_do__tabs-item';
	var activeClassName = 'what_we_do__tabs-item--active';

	pureTabs.init(className, activeClassName);

	new ScrollMagic.Scene({
			triggerElement: "section.what_we_do",
			duration: 0
		})
		.setTween(new TimelineMax({
			tweens: [
				TweenMax.to("#menuBtn", 0.5, {
					display: 'block',
					opacity: '1',
					ease: Linear.easeNone
				}),
			],
			align: 'start'
		}))
		.setPin("#menuBtn")
		.addTo(window.SM.oMainController);


	new ScrollMagic.Scene({
			triggerElement: ".what_we_do__tabs",
			offset: 120,
			duration: 0,
			triggerHook: 0.6,
			reverse: false
		})
		.setTween(new TimelineMax({
			tweens: [
				TweenMax.from(".what_we_do__tabs-item", 1.2, {
					opacity: 0,
					bottom: window.app.sTextAppearanceDistance,
					ease: Power2.easeOut
				}),
				TweenMax.to(".what_we_do__tabs-item", 1.5, {
					borderColor: '#68c6ff',
					ease: Power2.easeOut
				}),
				TweenMax.from(".what_we_do__tabs-item--active", 1.5, {
					borderColor: 'transparent',
					background: 'transparent',
					color: '#fff',
					ease: Linear.easeNone
				}),
				TweenMax.from(".what_we_do__tabs-item--active path", 1.8, {
					fill: '#fff',
					ease: Linear.easeNone
				}),
				TweenMax.from(".what_we_do__tabs-item--active", 0.8, {
					color: '#fff',
					ease: Linear.easeNone
				}),

			],
			onComplete: function () {
				$('.what_we_do__tabs-item--active .what_we_do__tabs-icon path[style]').removeAttr('style');
				$('.what_we_do__tabs-item--active[style]').attr('style', 'border-color: #68c6ff')
			},
			align: 'start'
		}))
		.addTo(window.SM.oMainController);

	new ScrollMagic.Scene({
			triggerElement: ".what_we_do__tabs",
			offset: 260,
			duration: 00,
			triggerHook: 0.6,
			reverse: false
		})
		.setTween(new TimelineMax({
			tweens: [
				TweenMax.to(".what_we_do__descr-text", 2.5, {
					borderColor: '#68c6ff',
					delay: 5.5,
					ease: Linear.easeNone
				}),
				TweenMax.from(".what_we_do__full-descrWrapper", 1.8, {
					opacity: '0',
					bottom: window.app.sTextAppearanceDistance,
					delay: 4.5,
					ease: Power2.easeOut
				}),
			],
			align: 'start'
		}))
		.addTo(window.SM.oMainController);

	$('.what_we_do__slider').owlCarousel({
		autoHeight: true,
		items: 1,
		singleItem: true,
	});

	function setPanelHeight() {
		var max_col_height = 0;
		$('.what_we_do__full-descr').each(function () {
			if ($(this).height() > max_col_height) {
				max_col_height = $(this).height();
			}
		});

		$('.what_we_do__descr-text').height(max_col_height + 120);
	}

	setPanelHeight();

	$(window).resize(function () {
		setPanelHeight();
	});
});