$(document).ready(function() {
  new ScrollMagic.Scene({
    duration: 0,
    triggerElement: 'section.our_clients',
    triggerHook: 0.7
  })
    .setTween(
      TweenMax.from('section.our_clients h2', 1, {
        opacity: 0,
        bottom: window.app.sTextAppearanceDistance,
        ease: Power2.easeOut
      })
    )
    //	.addIndicators({name: "video netvision"})
    .addTo(window.SM.oMainController);

  var sSelector = '.our_clients .container-fluid > div';
  if (!window.matchMedia('all and (min-width: 1280px)').matches) {
    sSelector = '.our_clients .container-fluid .logotype';
  }

  $(sSelector).each(function(iIndex, oItem) {
    new ScrollMagic.Scene({
      duration: 0,
      triggerElement: oItem,
      triggerHook: 0.7
    })
      .setTween(TweenMax.from(oItem, 1, { opacity: 0, bottom: '-50px', ease: Power2.easeOut }))
      //	.addIndicators({name: "video netvision"})
      .addTo(window.SM.oMainController);
  });

  new ScrollMagic.Scene({
    duration: window.app.screenHeight / 2,
    triggerElement: 'section.contacts',
    triggerHook: 1
  })
    .setTween(
      TweenMax.to('section.our_clients, section.contacts', 1.5, { backgroundColor: 'white' })
    )
    .addTo(window.SM.oMainController);
});
