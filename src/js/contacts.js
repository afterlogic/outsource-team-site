$(document).ready(function() {
	new ScrollMagic.Scene({
			duration: 0,
			triggerElement: "section.contacts",
			triggerHook: 0.4
		})
		.setTween(new TimelineMax({
    			tweens: [
					TweenMax.from(".contacts .address", 2, {opacity: 0, ease: Power2.easeOut}),
					TweenMax.from(".contacts .form", 2, {opacity: 0, ease: Power2.easeOut})
				]
    			// align: 'start'
    		}))
		// .addIndicators({name: ".contacts"})
		.addTo(window.SM.oMainController);
	
	
	new ScrollMagic.Scene({
			duration: 0,
			triggerElement: "section.contacts",
			triggerHook: 0.2
		})
		.setTween(TweenMax.from("section.contacts .main_logo", 2, {opacity: 0, delay: 1}))
		// .addIndicators({name: ".contacts"})
		.addTo(window.SM.oMainController);
	
	$.formUtils.addValidator({
		name : 'email',
		validatorFunction : function(value, $el, config, language, $form) {
			return true;
		},
		errorMessage : '',
		errorMessageKey: 'badEvenNumber'
	});
	$.formUtils.addValidator({
		name : 'lastname',
		validatorFunction : function (sText) {
			var emailParts = sText.toLowerCase().split('@'),
				localPart = emailParts[0],
				domain = emailParts[1];
			if (localPart && domain) {
				if( localPart.indexOf('"') === 0 ) {
				  var len = localPart.length;
				  localPart = localPart.replace(/\"/g, '');
				  if( localPart.length !== (len-2) ) {
					return false; // It was not allowed to have more than two apostrophes
				  }
				}

				return $.formUtils.validators.validate_domain.validatorFunction(emailParts[1]) &&
				  localPart.indexOf('.') !== 0 &&
				  localPart.substring(localPart.length-1, localPart.length) !== '.' &&
				  localPart.indexOf('..') === -1 &&
				  !(/[^\w\+\.\-\#\-\_\~\!\$\&\'\(\)\*\+\,\;\=\:]/.test(localPart));
			}
			
			return false;
		},
		errorMessage : 'Please provide a correct email',
		errorMessageKey: 'badEmail'
	});
	
	$.validate({
		form : '#contact_form',
		validateOnBlur: true,
		inlineErrorMessageCallback: function($input, errorMessage, config) {
			// console.log($input);
			// return true;
			 // Return an element that should contain the error message.
			 // This callback will called when validateOnBlur is set to true (default) and/or errorMessagePosition is set to 'inline'
		},
		submitErrorMessageCallback: function($form, errorMessages, config) {
			// Return an element that should contain all error messages.
			// This callback will be called when errorMessagePosition is set to 'top'
		}
		// onError : function($form) {
			// alert('Validation of form '+$form.attr('id')+' failed!');
			// return true;
		// },
		// onSuccess : function($form) {
			// alert('The form '+$form.attr('id')+' is valid!');
			// return false; // Will stop the submission of the form
		// }
	});
	
	var 
		options = {
			'hashTracking': false
		},
		oPopup = $('[data-remodal-id=modal]').remodal(options),
		oPopupCont = $('.remodal'),
		oButton = oPopupCont.find('.btn'),
		oText = oPopupCont.find('.text')
	;
	
	$('section.contacts form').bind('submit', function (event) {
		event.preventDefault();
		var sData = $( this ).serialize() + "&ajax=1";
		
		if (gtag) {
			gtag('event', 'Confirm', {'event_category': 'Button'});
		}
		
		$.post( "/", sData, function( data ) {
			if (data.result)
			{
				oButton.removeClass('btn-warning').addClass('btn-success');
				oText.html("Thank you!<br /> The message was sent successfully.<br /> We will contact you as soon as possible.");
			}
			else
			{
				oButton.removeClass('btn-success').addClass('btn-warning');
				oText.html('Oops, some problem has occurred.<br /> Please try again.');
				console.log('Error: ' + data.error);
			}

			oPopup.open();
		}, 'json');
		
		return false;
	});
});
