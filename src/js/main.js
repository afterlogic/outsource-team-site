$(document).ready(function() {
  var oOwlCarouselConfig = {
    transitionStyle: 'fade',
    items: 1,
    singleItem: true,
    navigation: true,
    navigationText: ['', 'See more details &rarr;'],
    pagination: true,
    autoHeight: true
  };

  if (window.matchMedia('all and (min-width: 1025px)').matches) {
    oOwlCarouselConfig.pagination = true;
    oOwlCarouselConfig.autoHeight = false;
  }
  var owl = $('.slider');
  owl.owlCarousel(oOwlCarouselConfig);

  $('.cases__item').on('mousewheel', function(e) {
    var curr = $(this)
      .closest('.cases__item')
      .find('.slider');

    var deltaY = Math.abs(e.deltaY);

    if (e.deltaX >= 1 && deltaY === 0) {
      _.debounce(curr.trigger('owl.next'), 20);
    } else if (e.deltaX <= -1 && deltaY === 0) {
      _.debounce(curr.trigger('owl.prev'), 20);
    }
  });

  $('.cases__nextBtn').click(function() {
    $(this)
      .closest('.slider')
      .trigger('owl.next');
  });

  window.app = {
    screenHeight: $(window).height(),
    sTextAppearanceDistance: '-50px'
  };

  if (window.matchMedia('all and (min-width: 1025px)').matches) {
    //		$('section').css("height", window.app.screenHeight);
    $(
      'section.what_we_do, section.how_we_work, section.our_clients, section.contacts'
    ).css('min-height', window.app.screenHeight);
    $(
      'section.splash, section.xura, section.medicity, section.citybooq, section.netvision'
    ).css('height', window.app.screenHeight);

    $(window).resize(function() {
      window.app.screenHeight = $(window).height();
      $(
        'section.what_we_do, section.how_we_work, section.our_clients, section.contacts'
      ).css('min-height', window.app.screenHeight);
      $(
        'section.splash, section.xura, section.medicity, section.citybooq, section.netvision'
      ).css('height', window.app.screenHeight);
    });
  } else {
    $('section').css('min-height', window.app.screenHeight);

    $(window).resize(function() {
      window.app.screenHeight = $(window).height();
      $('section').css('min-height', window.app.screenHeight);
    });
  }

  $('.cases__item').each(function() {
    new ScrollMagic.Scene({
      duration: 0,
      triggerElement: this,
      triggerHook: 0.3
    })
      .setTween(
        new TimelineMax({
          tweens: [
            TweenMax.from($(this).find('.cases__logo'), 0.8, {
              opacity: 0,
              y: 50,
              ease: Power2.easeOut
            }),
            TweenMax.from($(this).find('.cases__slide'), 0.8, {
              opacity: 0,
              y: 50,
              ease: Power2.easeOut
            })
          ]
        })
      )
      .addTo(window.SM.oMainController);

    new ScrollMagic.Scene({
      duration: 0,
      triggerElement: this,
      triggerHook: 0.35
    })
      .setTween(
        new TimelineMax({
          tweens: [
            TweenMax.from($(this).children('.cases__img'), 0.8, {
              opacity: 0,
              bottom: 450,
              ease: Power2.easeOut
            })
          ]
        })
      )
      .addTo(window.SM.oMainController);
  });
  $('.tech').each(function() {
       new ScrollMagic.Scene({
      duration: 0,
      triggerElement: this,
      triggerHook: 0.35
    })
      .setTween(
        new TimelineMax({
          tweens: [
            TweenMax.from($(this).find('.tech__item'), 0.8, {
              opacity: 0,
              transform: 'translateY(100px)',
              ease: Power2.easeOut
            })
          ]
        })
      )
      //.setClassToggle('.menu__btn', 'black')
      .addTo(window.SM.oMainController);
  })
});
