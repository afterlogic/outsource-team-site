<?php 
$sStaticDataHash = 6;
require_once ('general/functions.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>404 | Afterlogic Works</title>
    <?php echo setHtmlBaseUrl();
    include('gtag-manager.php'); ?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Afterlogic Works! We are full-stack web development agency. US jurisdiction. Cost-effective developers in
		Eastern Europe.</title>
	<meta name="description" content="404 page not found">
    <meta name="keywords" content="404, page not found">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php include('yandex-verification.php'); ?>
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/<?= asset_path('main.css') ?>">
	<link href="https://fonts.googleapis.com/css?family=Lora|Kavivanar" rel="stylesheet" type="text/css">
	<script src="js/libs.js"></script>
	<!--if lt IE 9script(src='js/vendor/html5-3.6-respond-1.4.2.min.js')
	-->
	<?php include('yandex-metrics.php'); ?>
</head>

<body>
	<?php include('gtag-iframe.php'); ?>
	<div class="main_content">
		<section class="splash"><img src="images/tri3.svg" id="el5"><img src="images/tri3.svg" id="el6"><img
				src="images/tri2.svg" id="el2"><img src="images/tri2.svg" id="el4"><img src="images/tri1.svg" id="el1"><img
				src="images/tri4.svg" id="el3">
			<div id="cover"></div>
			<div id="mask"></div>
			<div class="wrapper">
                <a href="/"><div class="main_logo"></div> </a>
				<div class="title errortitle"><span class="text"></span><span class="cursor"></span></div>
				<div class="description">
					<p></p>
                    <p>Page not found</p>
                    <a href="/"> <p>Homepage</p> </a>
				</div>				
			</div>
		</section>
        </div>
        <?php include('linkedin-analytics.php'); ?>
	<script type="text/javascript" src="https://widget.clutch.co/static/js/widget.js"></script>
	<script src="js/script.js?v=<?php echo $sStaticDataHash; ?>"></script>
</body>
</html>


