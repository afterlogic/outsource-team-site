<?php $sStaticDataHash = 3; ?><!DOCTYPE html>
<html lang="en">
<head>
	<?php include('gtag-manager.php'); ?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Afterlogic Works! We are full-stack web development agency. US jurisdiction. Cost-effective developers in Eastern Europe.</title>
	<meta name="description" content="We are full-stack web development agency. US jurisdiction. Cost-effective developers in Russia. Historically, Russian and former Soviet engineering school has been widely recognized and accepted all around the globe. Afterlogic's goal is to leverage the power of Russian tech spirit still keeping the peace of mind of our customers by providing USA jurisdiction to all the operations. We bring you the expertise of team of russian experts, but you work with USA-based company.">
	<meta name="keywords" content="web development agency, full-stack web development agency, outsource to russia, outsourcing to russia, php development, full-stack development, front-end development, software development, outsource development, custom development, web application, collaboration system development">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/main.css?v=<?php echo $sStaticDataHash; ?>">
	<link href="https://fonts.googleapis.com/css?family=Lora|Kavivanar" rel="stylesheet" type="text/css">
	<!--<script src="js/libs.js"></script>
	<script src="js/script.js?v=<?php echo $sStaticDataHash; ?>"></script>-->
	<!--if lt IE 9script(src='js/vendor/html5-3.6-respond-1.4.2.min.js')
	-->
</head>
<body>
	<?php include('gtag-iframe.php'); ?>
	<div class="main_content">
		<section class="text">
			<div class="container-fluid">
				<h2 class="title">Privacy policy</h2>
				<div class="description">
					<p>Afterlogic Corporation is committed to protecting visitor privacy online. It is Afterlogic Corporation policy that personal information, such as your name, postal and e-mail address or telephone number, is private and confidential. Accordingly, the personal information you provide is stored in a secure location accessible only by designated staff, and is used only for the purposes for which you provide the information.</p>
					<p>Our guiding principle is that we will never sell, lend or distribute email, business or personal addresses to any other company or organization for any purpose whatsoever. Neither will we ourselves, bombard you with unsolicited email.</p>
					<p>If you have any questions, concerns or comments, please call us at +1-415-513-0152.</p>
				</div>
			</div>
		</section>
	</div>
	<?php include('linkedin-analytics.php'); ?>
</body>
</html>