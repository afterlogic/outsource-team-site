<?php
$sStaticDataHash = 6;
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php include('gtag-manager.php'); ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Afterlogic.Works | A full-stack web&app development agency</title>
        <meta name="description"
              content="We specialize in corporate mail, private cloud file storage, data privacy and encryption, notification and messaging systems, data synchronization">
        <meta name="keywords" 
              content="web development agency, full stack web development agency, php development, full stack development, front end development, software development, outsource development, custom development, web application, collaboration system development">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Schema.org for Google -->
        <meta itemprop="name" content="A full-stack web&app development agency | Afterlogic.Works ">
        <meta itemprop="description" content="We specialize in corporate mail, private cloud file storage, data privacy and encryption, notification and messaging systems, data synchronization">
        <!-- Twitter -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:title" content="A full-stack web&app development agency | Afterlogic.Works ">
        <meta name="twitter:description" content="We specialize in corporate mail, private cloud file storage, data privacy and encryption, notification and messaging systems, data synchronization">
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
        <meta name="og:title" content="A full-stack web&app development agency | Afterlogic.Works ">
        <meta name="og:description" content="We specialize in corporate mail, private cloud file storage, data privacy and encryption, notification and messaging systems, data synchronization">
        <meta name="og:url" content="https://afterlogic.works/">
        <meta name="og:site_name" content="Afterlogic.Works">
        <meta name="og:locale" content="en_US">
        <meta name="og:type" content="website">
        <?php include('yandex-verification.php'); ?>
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/<?= asset_path('main.css') ?>">
        <link href="https://fonts.googleapis.com/css?family=Lora|Kavivanar" rel="stylesheet" type="text/css">
        <script src="js/libs.js"></script>
        <!--if lt IE 9script(src='js/vendor/html5-3.6-respond-1.4.2.min.js')
        -->
        <?php include('yandex-metrics.php'); ?>
    </head>

    <body>
				<?php include('gtag-iframe.php'); ?>
        <div class="main_content">
            <div id="menuBtn" class="menu__btn"></div>
            <div class="menu" id="menu">
                <ul class="menu__list">
                    <li class="menu__item"><a href="#what_we_do" class="menu__link">About</a></li>
                    <li class="menu__item"><a href="#specializations" class="menu__link">Specializations</a></li>
                    <li class="menu__item"><a href="#cases" class="menu__link">Cases</a></li>
                    <li class="menu__item"><a href="#how_we_work" class="menu__link">Process</a></li>
                    <li class="menu__item"><a href="#reviews" class="menu__link">Reviews</a></li>
                    <li class="menu__item"><a href="#clients" class="menu__link">Clients</a></li>
                    <li class="menu__item"><a href="http://afterlogic.medium.com" class="menu__link" target="_blank">Blog</a></li>
                    <li class="menu__item"><a href="#contacts" class="menu__link">Contact</a></li>
                </ul>
                <div class="menu__contact">
                    <div class="menu__group">
                        <div class="menu__group-wrapper">
                            <div class="menu__group-item">
                                <div class="menu__group-title">Headquarters</div>
                                <div class="menu__group-text">
                                    3411 Silverside Road,</br> Tatnall Building,
                                    Suite 104,</br> Wilmington, DE 19810 USA
                                </div>
                            </div>
                            <div class="menu__group-item">
                                <div class="menu__group-title">R&D</div>
                                <div class="menu__group-text">347900, Petrovskaya 89B, Taganrog, Russia</div>
                            </div>
                        </div>
                    </div>
                    <div class="menu__group">
                        <div class="menu__group-text">+1-415-513-0152</div>
                    </div>
                    <div class="menu__group">
                        <div class="menu__group-text"><a href="mailto:works@afterlogic.com"
                                                         class="menu__group-link">works@afterlogic.com</a></div>
                    </div>
                </div>
            </div>
            <nav class="navbar">
                <ul class="navbar__list">
                    <li class="navbar__item"><a href="#what_we_do" class="navbar__link">about</a></li>
                    <li class="navbar__item"><a href="#specializations" class="navbar__link">Specializations</a></li>
                    <li class="navbar__item"><a href="#cases" class="navbar__link">cases</a></li>
                    <li class="navbar__item"><a href="#how_we_work" class="navbar__link">process</a></li>
                    <li class="navbar__item"><a href="#reviews" class="navbar__link">reviews</a></li>
                    <li class="navbar__item"><a href="#clients" class="navbar__link">clients</a></li>
					<li class="navbar__item"><a href="http://afterlogic.medium.com" class="navbar__link" target="_blank">blog</a></li>
                    <li class="navbar__item"><a href="#contacts" class="navbar__link">contact</a></li>
                </ul>
            </nav>
            <div class="menu__overlay"></div>
            <section class="splash"><img src="images/tri3.svg" id="el5"><img src="images/tri3.svg" id="el6"><img
                    src="images/tri2.svg" id="el2"><img src="images/tri2.svg" id="el4"><img src="images/tri1.svg" id="el1"><img
                    src="images/tri4.svg" id="el3">
                <div id="cover"></div>
                <div id="mask"></div>
                <div class="wrapper">
                    <div class="main_logo"></div>
                    <div class="title"><span class="text"></span><span class="cursor"></span></div>
                    <div class="description">
                        <p>We are full-stack web development agency.</p>
                        <p>US jurisdiction. Cost-effective developers in Eastern Europe.</p>
                    </div>
                    <div class="learn_more">Learn more</div>
                    <div class="mouse">
                        <div></div>
                    </div>
                </div>
            </section>
            <section class="what_we_do" id="what_we_do">
                <div class="wrapper">
                    <div class="text_wrapper">
                        <h2 class="title">What we do</h2>
                        <div class="what_we_do__descr">
                            <div class="what_we_do__tabs">
                                <a href="#tab_web" class="what_we_do__tabs-item what_we_do__tabs-item--active">
                                    <div class="what_we_do__tabs-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="89" height="61">
                                        <path fill-rule="evenodd" fill="#FFF"
                                              d="M78.955 60.25H10.916c-5.503 0-9.979-4.451-9.979-9.923a1 1 0 0 1 1.002-.997h8.206a.963.963 0 0 1-.176-.531V9.046c0-4.576 3.744-8.299 8.346-8.299h53.24c4.602 0 8.346 3.723 8.346 8.299v39.753c0 .2-.074.375-.176.531h8.207a1 1 0 0 1 1.002.997c0 5.472-4.477 9.923-9.979 9.923zm-1.059-11.451V9.046c0-3.477-2.844-6.305-6.341-6.305h-53.24c-3.496 0-6.341 2.828-6.341 6.305v39.753c0 .2-.074.375-.176.531h66.274a.963.963 0 0 1-.176-.531zM3.004 51.324c.495 3.904 3.854 6.932 7.912 6.932h68.039c4.057 0 7.416-3.028 7.911-6.932H3.004zm24.967-31.137l2.105-3.626c.244-.42.757-.605 1.216-.437l4.22 1.55a13.59 13.59 0 0 1 5.572-3.212l.758-4.393a1 1 0 0 1 .988-.829h4.211c.488 0 .906.35.989.829l.757 4.393a13.606 13.606 0 0 1 5.573 3.212l4.218-1.55c.458-.168.971.017 1.216.437l2.106 3.626a.994.994 0 0 1-.228 1.265l-3.473 2.871a13.514 13.514 0 0 1-.017 6.358l3.489 2.882a.993.993 0 0 1 .228 1.265l-2.105 3.627a1.006 1.006 0 0 1-1.216.437l-4.28-1.572a13.608 13.608 0 0 1-5.497 3.149l-.771 4.478a1.004 1.004 0 0 1-.989.829H42.83a1 1 0 0 1-.988-.829l-.772-4.478a13.608 13.608 0 0 1-5.497-3.149l-4.281 1.572a1.006 1.006 0 0 1-1.216-.437l-2.105-3.625a.995.995 0 0 1 .227-1.266l3.489-2.883a13.472 13.472 0 0 1-.015-6.359l-3.474-2.87a.993.993 0 0 1-.227-1.265zm5.474 3.008a.994.994 0 0 1 .322 1.045 11.48 11.48 0 0 0 .018 6.516.993.993 0 0 1-.321 1.05l-3.333 2.755 1.259 2.169 4.087-1.501a1.009 1.009 0 0 1 1.071.245 11.61 11.61 0 0 0 5.638 3.229 1 1 0 0 1 .752.801l.737 4.278h2.521l.737-4.278a1 1 0 0 1 .752-.801 11.615 11.615 0 0 0 5.638-3.229 1.01 1.01 0 0 1 1.071-.245l4.086 1.501 1.259-2.17-3.333-2.754a.994.994 0 0 1-.32-1.05 11.514 11.514 0 0 0 .017-6.517.995.995 0 0 1 .323-1.044l3.314-2.739-1.26-2.17-4.029 1.48a1.007 1.007 0 0 1-1.076-.249 11.608 11.608 0 0 0-5.703-3.288 1 1 0 0 1-.753-.801l-.723-4.194h-2.521l-.723 4.194a.998.998 0 0 1-.753.801 11.608 11.608 0 0 0-5.703 3.288 1.01 1.01 0 0 1-1.076.249l-4.03-1.48-1.259 2.17 3.314 2.739zm11.491-2.118c3.566 0 6.468 2.885 6.468 6.431 0 3.546-2.902 6.432-6.468 6.432-3.567 0-6.47-2.886-6.47-6.432 0-3.546 2.903-6.431 6.47-6.431zm0 10.869c2.46 0 4.463-1.991 4.463-4.438 0-2.447-2.003-4.437-4.463-4.437-2.462 0-4.464 1.99-4.464 4.437s2.002 4.438 4.464 4.438z" />
                                        </svg>
                                    </div>
                                    <div class="what_we_do__tabs-title">Web development</div>
                                    <div class="what_we_do__tabs-text">Delivering high-quality web development leveraging the deep expertise
                                        of our team.</div>
                                </a>
                                <a href="#tab_mobile" class="what_we_do__tabs-item">
                                    <div class="what_we_do__tabs-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="65" height="81">
                                        <path fill-rule="evenodd" fill="#FFF"
                                              d="M46.204 37.353c-.172 0-.342-.004-.512-.009a1.04 1.04 0 0 1-.141.009h-4.776v36.552c0 3.499-2.864 6.345-6.385 6.345H6.509c-3.521 0-6.385-2.846-6.385-6.345V20.426c0-3.499 2.864-6.346 6.385-6.346h21.989C30.684 6.399 37.785.748 46.204.748c10.154 0 18.415 8.211 18.415 18.303s-8.261 18.302-18.415 18.302zM2.131 73.905c0 2.399 1.963 4.35 4.378 4.35H34.39c2.414 0 4.378-1.951 4.378-4.35V72.28H2.131v1.625zm0-3.62h36.637V37.353H27.882c-.541 0-.984-.426-1.003-.964a1 1 0 0 1 .935-1.029c.923-.065 3.728-.564 4.464-1.95.36-.679.203-1.603-.469-2.747a9.196 9.196 0 0 0-.499-.757c-.317-.442-.547-.81-.698-1.122a18.168 18.168 0 0 1-1.993-4.329H2.131v45.83zm4.378-54.209c-2.415 0-4.378 1.951-4.378 4.35v2.034h25.99a18.211 18.211 0 0 1-.331-3.409c0-1.015.105-2.005.265-2.975H6.509zM46.204 2.743c-9.047 0-16.406 7.316-16.406 16.308 0 3.096.879 6.11 2.543 8.718a.922.922 0 0 1 .066.119c.064.142.211.405.535.855.233.323.435.631.602.917 1.044 1.779 1.215 3.356.507 4.686-.205.384-.47.719-.777 1.012h12.069a.961.961 0 0 1 .179-.017l.042.001c.212.009.425.016.64.016 9.048 0 16.408-7.316 16.408-16.307 0-8.992-7.36-16.308-16.408-16.308zm10.112 13.338l-1.84 1.52a8.32 8.32 0 0 1-.008 3.41l1.846 1.526c.376.31.473.845.228 1.266l-1.233 2.122a1.007 1.007 0 0 1-1.217.437l-2.267-.832a8.403 8.403 0 0 1-2.948 1.688l-.409 2.371c-.082.48-.5.83-.989.83h-2.467c-.489 0-.907-.35-.99-.83l-.408-2.371a8.39 8.39 0 0 1-2.947-1.688l-2.269.832a1.004 1.004 0 0 1-1.216-.437l-1.234-2.122a.992.992 0 0 1 .227-1.266l1.848-1.526a8.361 8.361 0 0 1-.009-3.41l-1.839-1.52a.993.993 0 0 1-.227-1.266l1.234-2.123c.243-.42.756-.605 1.217-.437l2.23.819a8.405 8.405 0 0 1 2.993-1.724l.4-2.322c.083-.479.501-.83.99-.83h2.467c.489 0 .907.351.989.83l.4 2.322a8.387 8.387 0 0 1 2.993 1.724l2.23-.819c.461-.168.972.017 1.218.437l1.234 2.123a.994.994 0 0 1-.227 1.266zm-2.322-1.663l-2.031.746c-.377.139-.801.04-1.077-.25a6.383 6.383 0 0 0-3.137-1.807 1 1 0 0 1-.753-.802l-.363-2.111h-.775l-.364 2.111a1.002 1.002 0 0 1-.754.802 6.39 6.39 0 0 0-3.137 1.807c-.276.291-.698.39-1.076.25l-2.031-.745-.386.666 1.67 1.381a.996.996 0 0 1 .323 1.044 6.331 6.331 0 0 0 .01 3.582.996.996 0 0 1-.321 1.052l-1.682 1.389.386.666 2.066-.757a1.006 1.006 0 0 1 1.072.245 6.376 6.376 0 0 0 3.1 1.775c.389.094.685.409.753.802l.371 2.16h.775l.371-2.16c.068-.393.364-.708.753-.802a6.396 6.396 0 0 0 3.103-1.775 1.007 1.007 0 0 1 1.071-.245l2.063.757.386-.666-1.682-1.389a.997.997 0 0 1-.321-1.051 6.299 6.299 0 0 0 .264-1.808 6.41 6.41 0 0 0-.253-1.776.993.993 0 0 1 .322-1.043l1.672-1.381-.388-.667zm-7.749 9.071c-2.319 0-4.205-1.875-4.205-4.18 0-2.305 1.886-4.18 4.205-4.18s4.205 1.875 4.205 4.18c0 2.305-1.886 4.18-4.205 4.18zm0-6.364c-1.211 0-2.198.98-2.198 2.184 0 1.205.987 2.185 2.198 2.185 1.212 0 2.198-.98 2.198-2.185a2.194 2.194 0 0 0-2.198-2.184zM24.767 75.55h-8.636a1.001 1.001 0 0 1-1.004-.998c0-.551.45-.998 1.004-.998h8.636a1 1 0 0 1 1.004.998 1 1 0 0 1-1.004.998z" />
                                        </svg>

                                    </div>
                                    <div class="what_we_do__tabs-title">Mobile development</div>
                                    <div class="what_we_do__tabs-text">
                                        We develop iOS and Android mobile applications to reach more customers for your business.
                                    </div>
                                </a>
                                <a href="#tab_product" class="what_we_do__tabs-item">
                                    <div class="what_we_do__tabs-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="73" height="59">
                                        <path fill-rule="evenodd" fill="#FFF"
                                              d="M65.32 58.25H7.549c-4.06 0-7.363-3.294-7.363-7.343V8.093C.186 4.044 3.489.75 7.549.75H65.32c4.06 0 7.363 3.294 7.363 7.343v42.814c0 4.049-3.303 7.343-7.363 7.343zm-57.771-2H65.32c2.954 0 5.358-2.397 5.358-5.343v-38.29H2.191v38.29c0 2.946 2.403 5.343 5.358 5.343zM65.32 2.75H7.549c-2.955 0-5.358 2.397-5.358 5.343v2.524h68.487V8.093c0-2.946-2.404-5.343-5.358-5.343zm-4.77 6.4a2.294 2.294 0 0 1-2.297-2.291 2.294 2.294 0 0 1 2.297-2.29 2.294 2.294 0 0 1 2.297 2.29A2.294 2.294 0 0 1 60.55 9.15zm-7.95 0a2.294 2.294 0 0 1-2.297-2.291 2.294 2.294 0 0 1 2.297-2.29 2.294 2.294 0 0 1 2.297 2.29A2.294 2.294 0 0 1 52.6 9.15zM17.628 21.254l-9.124 4.562v.045l9.124 4.563v1.605L6.801 26.467v-1.256l10.827-5.564v1.607zm9.621-5.167l-6.534 16.872h-1.563l6.51-16.872h1.587zM39.551 26.49l-10.827 5.539v-1.605l9.194-4.563v-.045l-9.194-4.562v-1.607l10.827 5.539v1.304z" />
                                        </svg>

                                    </div>
                                    <div class="what_we_do__tabs-title">Products integration</div>
                                    <div class="what_we_do__tabs-text">Creating world-leading email and telecommunications components,
                                        software, and platforms since 2002.</div>
                                </a>
                            </div>
                            <div class="what_we_do__descr-text">
                                <div id="tab_web" class="what_we_do_full-descrWrapper active">
                                    <div class="what_we_do__full-descr">
                                        With a vast experience across the full web-services development lifecycle and more than 50 projects
                                        completed, Afterlogic.Works serves as a full stack web application development company for market
                                        leaders and emerging tech startups.
                                    </div>
                                </div>
                                <div id="tab_mobile" class="what_we_do_full-descrWrapper">
                                    <div class="what_we_do__full-descr">
                                        Full stack mobile development, from designing UX/UI of your application to coding it completely and
                                        creating a server back-end if required.
                                    </div>
                                </div>
                                <div id="tab_product" class="what_we_do_full-descrWrapper">
                                    <div class="what_we_do__full-descr">
                                        We have deep expertise in email, groupware, cloud storage, and highload systems. We can assist you
                                        with
                                        your business needs, be it a customization of our products, maintenance, deployment, or data migration
                                        issues.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="what_we_do__slider swiper-container">
                            <div class="what_we_do__slider-item">
                                <div class="what_we_do__slider-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="89" height="61">
                                    <path fill-rule="evenodd" fill="#FFF"
                                          d="M78.955 60.25H10.916c-5.503 0-9.979-4.451-9.979-9.923a1 1 0 0 1 1.002-.997h8.206a.963.963 0 0 1-.176-.531V9.046c0-4.576 3.744-8.299 8.346-8.299h53.24c4.602 0 8.346 3.723 8.346 8.299v39.753c0 .2-.074.375-.176.531h8.207a1 1 0 0 1 1.002.997c0 5.472-4.477 9.923-9.979 9.923zm-1.059-11.451V9.046c0-3.477-2.844-6.305-6.341-6.305h-53.24c-3.496 0-6.341 2.828-6.341 6.305v39.753c0 .2-.074.375-.176.531h66.274a.963.963 0 0 1-.176-.531zM3.004 51.324c.495 3.904 3.854 6.932 7.912 6.932h68.039c4.057 0 7.416-3.028 7.911-6.932H3.004zm24.967-31.137l2.105-3.626c.244-.42.757-.605 1.216-.437l4.22 1.55a13.59 13.59 0 0 1 5.572-3.212l.758-4.393a1 1 0 0 1 .988-.829h4.211c.488 0 .906.35.989.829l.757 4.393a13.606 13.606 0 0 1 5.573 3.212l4.218-1.55c.458-.168.971.017 1.216.437l2.106 3.626a.994.994 0 0 1-.228 1.265l-3.473 2.871a13.514 13.514 0 0 1-.017 6.358l3.489 2.882a.993.993 0 0 1 .228 1.265l-2.105 3.627a1.006 1.006 0 0 1-1.216.437l-4.28-1.572a13.608 13.608 0 0 1-5.497 3.149l-.771 4.478a1.004 1.004 0 0 1-.989.829H42.83a1 1 0 0 1-.988-.829l-.772-4.478a13.608 13.608 0 0 1-5.497-3.149l-4.281 1.572a1.006 1.006 0 0 1-1.216-.437l-2.105-3.625a.995.995 0 0 1 .227-1.266l3.489-2.883a13.472 13.472 0 0 1-.015-6.359l-3.474-2.87a.993.993 0 0 1-.227-1.265zm5.474 3.008a.994.994 0 0 1 .322 1.045 11.48 11.48 0 0 0 .018 6.516.993.993 0 0 1-.321 1.05l-3.333 2.755 1.259 2.169 4.087-1.501a1.009 1.009 0 0 1 1.071.245 11.61 11.61 0 0 0 5.638 3.229 1 1 0 0 1 .752.801l.737 4.278h2.521l.737-4.278a1 1 0 0 1 .752-.801 11.615 11.615 0 0 0 5.638-3.229 1.01 1.01 0 0 1 1.071-.245l4.086 1.501 1.259-2.17-3.333-2.754a.994.994 0 0 1-.32-1.05 11.514 11.514 0 0 0 .017-6.517.995.995 0 0 1 .323-1.044l3.314-2.739-1.26-2.17-4.029 1.48a1.007 1.007 0 0 1-1.076-.249 11.608 11.608 0 0 0-5.703-3.288 1 1 0 0 1-.753-.801l-.723-4.194h-2.521l-.723 4.194a.998.998 0 0 1-.753.801 11.608 11.608 0 0 0-5.703 3.288 1.01 1.01 0 0 1-1.076.249l-4.03-1.48-1.259 2.17 3.314 2.739zm11.491-2.118c3.566 0 6.468 2.885 6.468 6.431 0 3.546-2.902 6.432-6.468 6.432-3.567 0-6.47-2.886-6.47-6.432 0-3.546 2.903-6.431 6.47-6.431zm0 10.869c2.46 0 4.463-1.991 4.463-4.438 0-2.447-2.003-4.437-4.463-4.437-2.462 0-4.464 1.99-4.464 4.437s2.002 4.438 4.464 4.438z" />
                                    </svg>
                                </div>
                                <div class="what_we_do__slider-title">Web Development</div>
                                <div class="what_we_do__slider-descr">With a vast experience across the full web-services development
                                    lifecycle and more than 50 projects completed, Afterlogic.Works serves as a full stack web application
                                    development company for market leaders and emerging tech startups.
                                </div>
                            </div>
                            <div class="what_we_do__slider-item">
                                <div class="what_we_do__slider-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="65" height="81">
                                    <path fill-rule="evenodd" fill="#FFF"
                                          d="M46.204 37.353c-.172 0-.342-.004-.512-.009a1.04 1.04 0 0 1-.141.009h-4.776v36.552c0 3.499-2.864 6.345-6.385 6.345H6.509c-3.521 0-6.385-2.846-6.385-6.345V20.426c0-3.499 2.864-6.346 6.385-6.346h21.989C30.684 6.399 37.785.748 46.204.748c10.154 0 18.415 8.211 18.415 18.303s-8.261 18.302-18.415 18.302zM2.131 73.905c0 2.399 1.963 4.35 4.378 4.35H34.39c2.414 0 4.378-1.951 4.378-4.35V72.28H2.131v1.625zm0-3.62h36.637V37.353H27.882c-.541 0-.984-.426-1.003-.964a1 1 0 0 1 .935-1.029c.923-.065 3.728-.564 4.464-1.95.36-.679.203-1.603-.469-2.747a9.196 9.196 0 0 0-.499-.757c-.317-.442-.547-.81-.698-1.122a18.168 18.168 0 0 1-1.993-4.329H2.131v45.83zm4.378-54.209c-2.415 0-4.378 1.951-4.378 4.35v2.034h25.99a18.211 18.211 0 0 1-.331-3.409c0-1.015.105-2.005.265-2.975H6.509zM46.204 2.743c-9.047 0-16.406 7.316-16.406 16.308 0 3.096.879 6.11 2.543 8.718a.922.922 0 0 1 .066.119c.064.142.211.405.535.855.233.323.435.631.602.917 1.044 1.779 1.215 3.356.507 4.686-.205.384-.47.719-.777 1.012h12.069a.961.961 0 0 1 .179-.017l.042.001c.212.009.425.016.64.016 9.048 0 16.408-7.316 16.408-16.307 0-8.992-7.36-16.308-16.408-16.308zm10.112 13.338l-1.84 1.52a8.32 8.32 0 0 1-.008 3.41l1.846 1.526c.376.31.473.845.228 1.266l-1.233 2.122a1.007 1.007 0 0 1-1.217.437l-2.267-.832a8.403 8.403 0 0 1-2.948 1.688l-.409 2.371c-.082.48-.5.83-.989.83h-2.467c-.489 0-.907-.35-.99-.83l-.408-2.371a8.39 8.39 0 0 1-2.947-1.688l-2.269.832a1.004 1.004 0 0 1-1.216-.437l-1.234-2.122a.992.992 0 0 1 .227-1.266l1.848-1.526a8.361 8.361 0 0 1-.009-3.41l-1.839-1.52a.993.993 0 0 1-.227-1.266l1.234-2.123c.243-.42.756-.605 1.217-.437l2.23.819a8.405 8.405 0 0 1 2.993-1.724l.4-2.322c.083-.479.501-.83.99-.83h2.467c.489 0 .907.351.989.83l.4 2.322a8.387 8.387 0 0 1 2.993 1.724l2.23-.819c.461-.168.972.017 1.218.437l1.234 2.123a.994.994 0 0 1-.227 1.266zm-2.322-1.663l-2.031.746c-.377.139-.801.04-1.077-.25a6.383 6.383 0 0 0-3.137-1.807 1 1 0 0 1-.753-.802l-.363-2.111h-.775l-.364 2.111a1.002 1.002 0 0 1-.754.802 6.39 6.39 0 0 0-3.137 1.807c-.276.291-.698.39-1.076.25l-2.031-.745-.386.666 1.67 1.381a.996.996 0 0 1 .323 1.044 6.331 6.331 0 0 0 .01 3.582.996.996 0 0 1-.321 1.052l-1.682 1.389.386.666 2.066-.757a1.006 1.006 0 0 1 1.072.245 6.376 6.376 0 0 0 3.1 1.775c.389.094.685.409.753.802l.371 2.16h.775l.371-2.16c.068-.393.364-.708.753-.802a6.396 6.396 0 0 0 3.103-1.775 1.007 1.007 0 0 1 1.071-.245l2.063.757.386-.666-1.682-1.389a.997.997 0 0 1-.321-1.051 6.299 6.299 0 0 0 .264-1.808 6.41 6.41 0 0 0-.253-1.776.993.993 0 0 1 .322-1.043l1.672-1.381-.388-.667zm-7.749 9.071c-2.319 0-4.205-1.875-4.205-4.18 0-2.305 1.886-4.18 4.205-4.18s4.205 1.875 4.205 4.18c0 2.305-1.886 4.18-4.205 4.18zm0-6.364c-1.211 0-2.198.98-2.198 2.184 0 1.205.987 2.185 2.198 2.185 1.212 0 2.198-.98 2.198-2.185a2.194 2.194 0 0 0-2.198-2.184zM24.767 75.55h-8.636a1.001 1.001 0 0 1-1.004-.998c0-.551.45-.998 1.004-.998h8.636a1 1 0 0 1 1.004.998 1 1 0 0 1-1.004.998z" />
                                    </svg>
                                </div>
                                <div class="what_we_do__slider-title">Mobile Development</div>
                                <div class="what_we_do__slider-descr">Full stack mobile development, from designing UX/UI of your
                                    application to coding it completely and creating a server back-end if required.</div>
                            </div>
                            <div class="what_we_do__slider-item">
                                <div class="what_we_do__slider-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="73" height="59">
                                    <path fill-rule="evenodd" fill="#FFF"
                                          d="M65.32 58.25H7.549c-4.06 0-7.363-3.294-7.363-7.343V8.093C.186 4.044 3.489.75 7.549.75H65.32c4.06 0 7.363 3.294 7.363 7.343v42.814c0 4.049-3.303 7.343-7.363 7.343zm-57.771-2H65.32c2.954 0 5.358-2.397 5.358-5.343v-38.29H2.191v38.29c0 2.946 2.403 5.343 5.358 5.343zM65.32 2.75H7.549c-2.955 0-5.358 2.397-5.358 5.343v2.524h68.487V8.093c0-2.946-2.404-5.343-5.358-5.343zm-4.77 6.4a2.294 2.294 0 0 1-2.297-2.291 2.294 2.294 0 0 1 2.297-2.29 2.294 2.294 0 0 1 2.297 2.29A2.294 2.294 0 0 1 60.55 9.15zm-7.95 0a2.294 2.294 0 0 1-2.297-2.291 2.294 2.294 0 0 1 2.297-2.29 2.294 2.294 0 0 1 2.297 2.29A2.294 2.294 0 0 1 52.6 9.15zM17.628 21.254l-9.124 4.562v.045l9.124 4.563v1.605L6.801 26.467v-1.256l10.827-5.564v1.607zm9.621-5.167l-6.534 16.872h-1.563l6.51-16.872h1.587zM39.551 26.49l-10.827 5.539v-1.605l9.194-4.563v-.045l-9.194-4.562v-1.607l10.827 5.539v1.304z" />
                                    </svg>

                                </div>
                                <div class="what_we_do__slider-title">Products Integration</div>
                                <div class="what_we_do__slider-descr">We have deep expertise in email, groupware, cloud storage, and
                                    highload systems. We can assist you with your business needs, be it a customization of our products,
                                    maintenance, deployment, or data migration issues.</div>
                            </div>
                        </div>
                    </div>
            </section>
            <section class="our-specializations" id="specializations">
                <div class="our-specializations__title">
                    Our specializations
                </div>
                <div class="our-specializations__list">
                    <div class="our-specializations__row">
                        <div class="our-specializations__item">
                            <img src="/images/specializations/database.svg" alt="database" class="our-specializations__itemIcon">
                            <div class="our-specializations__itemTitle">Corporate Storages</div>
                            <div class="our-specializations__itemDescr">Development of self-hosted Corporate storages. Access from the
                                Web, DAV, Mobile Applications. Secured by strong encryption.</div>
                        </div>
                        <div class="our-specializations__item">
                            <img src="/images/specializations/messaging.svg" alt="messaging" class="our-specializations__itemIcon">
                            <div class="our-specializations__itemTitle">Corporate and Mass Messaging</div>
                            <div class="our-specializations__itemDescr">Self-hosted transactional and mass mailing services like
                                email/SMS/push-notifications. Highload solutions based on Docker.</div>
                        </div>
                        <div class="our-specializations__item">
                            <img src="/images/specializations/email.svg" alt="email" class="our-specializations__itemIcon">
                            <div class="our-specializations__itemTitle">Email and Webmails</div>
                            <div class="our-specializations__itemDescr">Messaging systems (webmails with threads, chats). Integration
                                with any
                                hosting control panel. Single Sign-On (SSO). Data synchronization.</div>
                        </div>
                    </div>
                    <div class="our-specializations__row">
                        <div class="our-specializations__item">
                            <img src="/images/specializations/privacy.svg" alt="privacy" class="our-specializations__itemIcon">
                            <div class="our-specializations__itemTitle">Privacy. Message encryption</div>
                            <div class="our-specializations__itemDescr">Browser-based encryption (AES-256). The server has no access to the encryption keys. Integration of peer-to-peer encryption with your platform.</div>
                        </div>
                        <div class="our-specializations__item">
                            <img src="/images/specializations/sync.svg" alt="sync" class="our-specializations__itemIcon">
                            <div class="our-specializations__itemTitle">Synchronization</div>
                            <div class="our-specializations__itemDescr">Data synchronization between your platforms, services,
                                applications,
                                mobile devices (contacts, calendars, files and folders). Integration with ActiveDir, LDAP, DAV.</div>
                        </div>
                        <div class="our-specializations__item">
                            <img src="/images/specializations/dev.svg" alt="dev" class="our-specializations__itemIcon">
                            <div class="our-specializations__itemTitle">Custom development</div>
                            <div class="our-specializations__itemDescr">Development, integration, and ongoing maintenance of web and mobile applications. More than 100 completed projects.</div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="cases" class="cases">
                <div class="cases__item cases__item_citybooq">
                    <div class="cases__leftSide">
                        <img src="/images/cases/citybooq_logo.png" alt="citybooq_logo" class="cases__logo">
                        <video autoplay loop muted poster="video/deep-blue.jpg">
                            <source src="video/deep-blue.mp4" type="video/mp4">
                            <source src="video/deep-blue.webm" type="video/webm">
                        </video>
                        <div class="cases__slider slider owl-carousel owl-theme">
                            <div class="cases__slide">
                                <div class="cases__text">
                                    Citibooq hired us to create an innovative web portal and iOS app for their touristic service which helps
                                    travellers find amazing places all over the world.
                                </div>

                            </div>
                            <div class="cases__slide">
                                <div class="cases__text">
                                    Web portal app we developed consists of a set of Single-Page-Application
                                    front-ends and JSON REST server as a backend.
                                </div>
                                <div class="cases__text cases__text_small">
                                    The portal app provides rich audio/video/imaging capabilities so we utilized a
                                    number of tools to organize media streaming/transcoding and applying various effects to images. The
                                    project is deeply integrated with social networks, geo-positioning, and mapping services.
                                </div>
                                <div class="cases__text cases__text_small">We also developed the iPhone application companion which uses
                                    the
                                    same JSON REST server as a backend.</div>
                                <!-- <div class="cases__technologies">
                                        <div class="cases__technologiesItem">
                                                <img src="images/technologies/png/php.svg">
                                        </div>
                                        <div class="cases__technologiesItem">
                                                REST
                                        </div>
                                        <div class="cases__technologiesItem">
                                                <img src="images/technologies/png/mysql.svg">
                                        </div>
                                        <div class="cases__technologiesItem">
                                                CDN
                                        </div>
                                        <div class="cases__technologiesItem">
                                                Objective C
                                        </div>
                                </div> -->

                            </div>
                            <div class="cases__slide">
                                <div class="cases__text">
                                    "AfterLogic was just a perfect choice of a professional services team for our start-up. Their
                                    competence, perfect timing and attention to details is what they are the best at. Keep up the great
                                    work and good luck!"
                                </div>
                                <div class="cases__text cases__text_small">
                                    Igor Saakyants, <br>CEO, Citibooq
                                </div>

                            </div>
                        </div>
                    </div>
                    <img src="/images/cases/citybooq_img.png" alt="citybooq_img" class="cases__img">
                </div>
                <div class="cases__item cases__item_medicity">
                    <div class="cases__leftSide">
                        <img src="/images/cases/medicity_logo.png" alt="medicity_logo" class="cases__logo">
                        <video autoplay loop muted poster="video/heart-rate.jpg">
                            <source src="video/heart-rate.mp4" type="video/mp4">
                            <source src="video/heart-rate.webm" type="video/webm">
                        </video>
                        <div class="cases__slider slider owl-carousel owl-theme">
                            <div class="cases__slide">
                                <div class="cases__text">
                                    Medicity hired us to develop a messaging system for healthcare industry.
                                </div>
                                <div class="cases__text">
                                    Based on our ASP.NET platform, the customized solution enabled "delegates" concept and custom storage
                                    of e-mails with REST API.
                                </div>

                            </div>
                            <div class="cases__slide">
                                <div class="cases__text">
                                    The solution developed for Medicity lets healthcare industry professionals
                                    work with their e-mail and delegate access to their accounts to other members.
                                </div>
                                <div class="cases__text cases__text_small">
                                    This solution supports all the typical features of a webmail client along with the
                                    ability to access delegated accounts, search through the available healthcare specialists, assign and
                                    manage delegations.
                                </div>
                                <div class="cases__text cases__text_small">
                                    REST API was developed to use Medicity's internal facilities of
                                    delivering messages into the system rather than plain POP3.
                                </div>

                            </div>
                            <div class="cases__slide">
                                <div class="cases__text">
                                    "Working with AfterLogic was a smooth experience. High-quality code, quick turnarounds, flexible
                                    minds. And project on time. <br>
                                    Always open to suggest ideas and discuss better ways of making things work, AfterLogic guys helped us
                                    meet all the challenges of this project. Even changing some original requirements for performance sake
                                    during the project lifecycle was no-brainer with AfterLogic and didn't have any impact on the budget."
                                </div>
                                <div class="cases__text cases__text_small">
                                    Lance Rodela, <br> Director, Product Management
                                </div>


                            </div>
                        </div>
                    </div>
                    <img src="/images/cases/medicity_img.png" alt="medicity_img" class="cases__img">
                </div>
                <div class="cases__item cases__item_mavenir">
                    <div class="cases__leftSide">
                        <img src="/images/cases/mavenir_logo.png" alt="mavenir_logo" class="cases__logo">
                        <video autoplay loop muted poster="video/square2.jpg">
                            <source src="video/square2.mp4" type="video/mp4;">
                            <source src="video/square2.webm" type="video/webm; codecs=&quot;vp8, vorbis&quot;">
                        </video>
                        <div class="cases__slider slider owl-carousel owl-theme">
                            <div class="cases__slide">
                                <div class="cases__text">
                                    We helped Acision build a number of modern web portals servicing millions of subscribers of the world
                                    leading
                                    mobile network operators.
                                </div>
                                <div class="cases__text">
                                    We also leveraged our expertise in communication protocols and technologies to optimize Acision's
                                    messaging infrastructure.
                                </div>

                            </div>
                            <div class="cases__slide">
                                <div class="cases__text">
                                    In this project we utilized a broad set of technologies, from user interface
                                    implementation to building custom Linux distributions for deep optimization of Acision backend
                                    infrastructure.
                                </div>
                                <div class="cases__text cases__text_small">
                                    <p>- MMS GUI development. Viewing/managing/transcoding MMS audio and video messages.</p>
                                    <p>- Voicemail and fax messages GUI, account settings.</p>
                                    <p>- Customer care GUI - a web tool for the operator's support staff. Includes lots of tools including
                                        user management, permission management, viewing system logs, etc.</p>
                                    <p>- Postfix MTA scripts for tracking message traffic.</p>
                                    <p>- Building custom Linux RPM packages for deployment of customized server components.</p>
                                </div>

                            </div>
                            <div class="cases__slide">
                                <div class="cases__text">
                                    We designed and implemented webmail interface front-end which uses Acision platform as a backend, with
                                    LDAP-based public address book. Also we performed branding of the user interface according the
                                    operator's standards.
                                </div>

                            </div>
                            <div class="cases__slide">
                                <div class="cases__text">
                                    Our task was to design and develop webmail interface which lets subscribers access voice and fax
                                    messages and configure their account settings.
                                </div>

                            </div>
                            <div class="cases__slide">
                                <div class="cases__text">
                                    <p>"Through all the years of our cooperation, AfterLogic has always been the reliable partner backed
                                        with a team of web and messaging technologies experts.</p>
                                    <p>AfterLogic's ability to provide on-site and online support and maintenance for up to 24x7 level is
                                        also the important factor for us, helping us build and run the platforms which serve hundreds of
                                        millions of people worldwide."</p>
                                </div>
                                <div class="cases__text cases__text_small">
                                    Sergey Buravov, <br> Senior Project Manager
                                </div>

                            </div>
                        </div>
                    </div>
                    <img src="/images/cases/mavenir_img.png" alt="mavenir_img" class="cases__img">
                </div>
                <div class="cases__item cases__item_netvision">
                    <div class="cases__leftSide">
                        <img src="/images/cases/netvision_logo.png" alt="netvision_logo" class="cases__logo">
                        <video autoplay loop muted poster="video/data-transfer.jpg">
                            <source src="video/data-transfer.mp4" type="video/mp4">
                            <source src="video/data-transfer.webm" type="video/webm; codecs=&quot;vp8, vorbis">
                        </video>
                        <div class="cases__slider slider owl-carousel owl-theme">
                            <div class="cases__slide">
                                <div class="cases__text">
                                    Netvision decided to choose us to build a web front-end for their mail server and contacts LDAP
                                    directory.
                                </div>
                                <div class="cases__text">
                                    Our experience in building user interfaces which support both Left-to-Right and Right-to-Left layouts
                                    was the important factor for this decision.
                                </div>

                            </div>
                            <div class="cases__slide">
                                <div class="cases__text">
                                    We optimized our PHP messaging solution to work in Netvision's cluster environment, added
                                    a custom authentication layer, enabled deep integration with their LDAP directory for accessing
                                    contacts, and branded the look-n-feel to conform to their corporate style.
                                </div>
                                <div class="cases__text cases__text_small">
                                    The platform now successfully serves about a million of Netvision subscribers, seamlessly integrated
                                    into their user portal.
                                </div>

                            </div>
                            <div class="cases__slide">
                                <div class="cases__text">
                                    "Over the years, AfterLogic has proven itself as not only the experienced technology company but also
                                    a long-term partner you can count on in a distant perspective.
                                    <br>
                                    For a large long-running project, it's vital to protect your investments and have peace of mind
                                    knowing that the team behind the project is always around and ready to help, whatever happens.
                                    AfterLogic shares the same values so we found it the right fit for this strategy."
                                </div>
                                <div class="cases__text cases__text_small">
                                    Valentin Zaharov, <br> System Group Manager
                                </div>

                            </div>
                        </div>
                    </div>
                    <img src="/images/cases/netvision_img.png" alt="netvision_img" class="cases__img">
                </div>
            </section>
            <section class="how_we_work" id="how_we_work">
                <div class="container-fluid">
                    <h2 class="title">How we work</h2>
                    <div class="description">We respect Agile and Scrum. We send you regular reports to keep you on track. We're
                        always here to hear you, be it phone, email, or Skype.</div>
                    <div class="stage stage1">
                        <div class="back"></div>
                        <div class="content"><img src="images/fly.png">
                            <p>Kickoff Meeting</p>
                        </div>
                    </div>
                    <div class="stage stage2">
                        <div class="back"></div>
                        <div class="content"><img src="images/research.png">
                            <p>Research and Negotiations</p>
                        </div>
                    </div>
                    <div class="stage stage3">
                        <div class="back"></div>
                        <div class="content"><img src="images/pen.png">
                            <p>Setting milestones</p>
                        </div>
                    </div>
                    <div class="stage stage4">
                        <div class="back"></div>
                        <div class="content"><img src="images/dev.png">
                            <p>Actual Development</p>
                        </div>
                    </div>
                    <div class="stage stage5">
                        <div class="back"></div>
                        <div class="content"><img src="images/qa.png">
                            <p>QA</p>
                        </div>
                    </div>
                    <div class="stage stage6">
                        <div class="back"></div>
                        <div class="content"><img src="images/plane.png">
                            <p>Release</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="tech">
                <div class="container">
                    <div class="title">
                        <h2 class="title">Technology stack:</h2>
                    </div>
                    <div class="tech__inner">
                        <div class="tech__item"><img
                                src="images/technologies/png/electron.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/sql.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/flutter.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/php.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/js.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/api.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/slim_php.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/simfony.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/node.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/typescript.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/laravel.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/vue.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/react.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/angular.png">
                        </div>
                        <div class="tech__item"><img
                                src="images/technologies/png/webpack.png">
                        </div>
                        <div class="tech__item "><img
                                src="images/technologies/png/postgress.png">
                        </div>
                    </div>
                </div>
            </section>
            <section class="feedback" id="reviews">
                <div class="clutch-widget" data-url="https://widget.clutch.co" data-widget-type="4" data-expandifr="true"
                     data-height="auto" data-snippets="true" data-clutchcompany-id="248844"></div>
            </section>
            <section class="our_clients" id="clients">
                <div class="container-fluid">
                    <h2 class="title">Some of our clients</h2>
                    <div>
                        <div class="logotype"><img src="images/clients/uol.png"></div>
                        <div class="logotype"><img src="images/clients/liveoffice.png"></div>
                        <div class="logotype"><img src="images/clients/gct.png"></div>
                        <div class="logotype"><img src="images/clients/NJCU.png" title="New Jersey City University"></div>
                    </div>
                    <div>
                        <div class="logotype"><img src="images/clients/delaware-tech.png"
                                                   title="Delaware Technical & Community College"></div>
                        <div class="logotype"><img src="images/clients/midco.png"
                                                   title="Midco | Internet, Cable TV, Home Phone &amp; Home Automation"></div>
                        <div class="logotype"><img src="images/clients/nuevo.png" title="Nuevo Yazilim Cozumleri AS"></div>
                        <div class="logotype"><img src="images/clients/leslies.png"></div>
                    </div>
                    <div>
                        <div class="logotype"><img src="images/clients/york-university.png" title="York University"></div>
                        <div class="logotype"><img src="images/clients/livecareer.png" title="LiveCareer: Resume Builder"></div>
                        <div class="logotype"><img src="images/clients/natro.png" title="Unlimited Web Hosting"></div>
                        <!--<div class="logotype"><img src="images/clients/sts.png" title="Special Telecommunications Service (STS), Romania"></div>-->
                    </div>
                </div>
            </section>

            <section class="contacts" id="contacts">
                <div data-remodal-id="modal">
                    <!--<button data-remodal-action="close" class="remodal-close"></button>-->
                    <p class="text"></p>
                    <br />
                    <span data-remodal-action="confirm" class="btn">OK</span>
                </div>
                <div class="background">
                    <object class="map" type="image/svg+xml" data="images/world.svg">
                        <img src="images/world.png" width="930" height="620" />
                    </object>
                </div>
                <div class="section-content">
                    <div class="description">
                        <div class="main_logo"></div>
                        <h2>Work with us</h2>
                        <br />
                        <br />
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 col-md-10 col-md-offset-1 col-lg-6 col-lg-offset-3">
                                <div class="row">
                                    <div class="col-md-5 address">
                                        <h3>Headquarters</h3>
                                        <p>3411 Silverside Road, Tatnall Building,
                                            Suite 104, Wilmington, DE 19810 USA</p>
                                        <p><a href="mailto:works@afterlogic.com" class="company_mail">works@afterlogic.com</a></p>
                                        <p><a href="tel:+1-415-513-0152" class="">+1-415-513-0152</a></p>
                                        <h3>R&amp;D</h3>
                                        <p>347900, Petrovskaya 89B, Taganrog, Russia</p>
                                        <p><a href="tel:+7-8634-311-240" class="">+7-8634-311-240</a></p>
                                        <br />
                                    </div>
                                    <div class="col-md-5 col-md-offset-2 form">
                                        <?php if ($result !== null) { ?>
                                            The message was successfully sent!
                                        <?php } ?>

                                        <form id="contact_form" method="POST">
                                            <input type="hidden" name="action" value="task.send" />
                                            <div class="form-group">
                                                <input name="name" type="text" class="form-control" placeholder="Name"
                                                       data-validation="required, length" data-validation-length="3-255"
                                                       data-validation-error-msg="Please provide your name">
                                            </div>
                                            <div class="form-group">
                                                <input name="lastname" type="text" class="form-control" placeholder="Email"
                                                       data-validation="required, lastname"
                                                       data-validation-error-msg="Please provide a your last name">
                                            </div>
                                            <div class="form-group">
                                                <input name="email" type="text" class="form-control" tabindex="-1" placeholder="Email"
                                                       data-validation="email" data-validation-error-msg="Please provide a correct email">
                                            </div>
                                            <div class="form-group">
                                                <input name="phone" type="text" class="form-control" placeholder="Phone">
                                            </div>
                                            <div class="form-group">
                                                <textarea name="message" class="form-control" placeholder="Describe you project " rows="6"
                                                          data-validation="required" data-validation-error-msg="Please provide any text"></textarea>
                                            </div>

                                            <input value="Send message" type="submit" class="btn btn-primary">
                                        </form>
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row footer">
                            <div class=" social col-sm-6 col-sm-offset-3 col-md-10 col-md-offset-1 col-lg-6 col-lg-offset-3">
                                <p class="social-list">
                                    <a href="https://linkedin.com/company/afterlogic-works" target="_blank" class="social-link"><img
                                            src="images/linkedin.png" /></a>
                                    <a href="https://www.facebook.com/Afterlogic.Works" target="_blank" class="social-link"><img
                                            src="images/facebook.png" /></a>
                                    <a href="https://clutch.co/profile/afterlogicworks" target="_blank" class="social-link"><img
                                            src="images/clutch.png" /></a>
                                </p>
                                <p class="social-list"><a href="/privacy-policy">Privacy policy</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <?php include('linkedin-analytics.php'); ?>
        <script type="text/javascript" src="https://widget.clutch.co/static/js/widget.js"></script>
        <script src="js/script.js?v=<?php echo $sStaticDataHash; ?>"></script>
    </body>

</html>