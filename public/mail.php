<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$Dir = dirName(__File__);

require $Dir.'/../vendor/PHPMailer/src/Exception.php';
require $Dir.'/../vendor/PHPMailer/src/PHPMailer.php';
require $Dir.'/../vendor/PHPMailer/src/SMTP.php';

function senMail($aData)
{
	$to      = 'a.nefedov@gmail.com,alex@afterlogic.com,works@afterlogic.com,s.kutenkov@afterlogic.com,support@afterlogic.com,vasil@afterlogic.com';
	$sSubject = 'Afterlogic Works Contact Form';
	$sMessage = "Time: \t\t" . $aData['time'] . "\r\n".
		"Name: \t\t" . $aData['name'] . "\r\n".
		"Email: \t\t" . $aData['email'] . "\r\n".
		"Phone: \t\t" . $aData['phone'] . "\r\n".
		"Message: \r\n" . $aData['message']
	;
	// headers = 'From: afterlogic.works' . "\r\n" .
	// 'X-Mailer: PHP/' . phpversion();
			
	file_put_contents('messages/message-'.str_replace(array(":", " "), array("-", "-"), $aData['time']), implode(array($sSubject, $sMessage), "\r\n\r\n"), FILE_APPEND);

	// return mail($to, $subject, $message, $headers);
	$mail = new PHPMailer(true); // Passing `true` enables exceptions
	
	try {
		//Server settings
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);
		// $mail->SMTPDebug = 4;                        // Enable verbose debug output
		$mail->isSMTP();                                // Set mailer to use SMTP
		$mail->Host = 'mail.afterlogic.com';			// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                         // Enable SMTP authentication
		$mail->Username = 'support@afterlogic.com';     // SMTP username
		$mail->Password = 'wax4bee';                    // SMTP password
		$mail->SMTPSecure = '';							// Enable TLS encryption, `ssl` also accepted
		$mail->Port = 25;                               // TCP port to connect to

        //Recipients
        $mail->setFrom('works@afterlogic.com', 'Afterlogic Works');
        
        if ($_SERVER['HTTP_HOST'] === 'afterlogic.works') {
            $mail->addAddress('a.nefedov@gmail.com');               // Name is optional
            $mail->addAddress('alex@afterlogic.com');               // Name is optional
            $mail->addAddress('works@afterlogic.com');               // Name is optional
            $mail->addAddress('s.kutenkov@afterlogic.com');               // Name is optional
            $mail->addAddress('support@afterlogic.com');               // Name is optional
            $mail->addAddress('vasil@afterlogic.com');               // Name is optional
            $mail->addAddress('i.lomakin@afterlogic.com');
        } else {
            // for tests
            $mail->addAddress('i.lomakin@afterlogic.com');
        }

        $mail->addReplyTo($aData['email'], $aData['name']);

		//Attachments
		//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		//Content
		$mail->CharSet = "utf-8";
		$mail->isHTML(false);                                  // Set email format to HTML
		$mail->Subject = $sSubject;
		$mail->Body    = $sMessage;
		// $mail->AltBody = $sMessage;
		$result = $mail->send();
		
		return $result;
	} catch (Exception $e) {
		return $mail->ErrorInfo; // . $mail->ErrorInfo;
	}
}
