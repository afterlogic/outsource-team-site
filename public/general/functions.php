<?php

/** set <base href=""> tag
 * @return string
 */
function setHtmlBaseUrl()
{
    $domain = isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] : 'http://' . $_SERVER['HTTP_HOST'];

    return '<base href=" '  . $domain . '">';
}
