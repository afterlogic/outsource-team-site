<?php     
	function asset_path($filename) {
		$manifest_path = './rev-manifest.json';
		if (file_exists($manifest_path)) {
			$manifest = json_decode(file_get_contents($manifest_path), TRUE);
		} else {
			$manifest = [];
		}
		if (array_key_exists($filename, $manifest)) {
			return $manifest[$filename];
		}
			return $filename;
	}
	
	$result = null;
	
	function getToken() {
		return md5(str_pad(mt_rand(1, 1000), 4, "0", STR_PAD_LEFT) . time());
	}

	if (isset($_POST['action']) && (string)$_POST['action'] === "task.send")
	{
		$bAjax = isset($_POST['ajax']) && (int)$_POST['ajax'] === 1 ? true : false;
		if ($bAjax)
		{
			header('Content-Type: application/json; charset=utf-8');
		}
		
		if ($_POST['email'] !== '')
		{
			// $token = $_SESSION["token"];
			// if (isset($_POST[$token])) {
				// $_SESSION["token"] = getToken();
			// }
			
			require dirName(__File__). '/mail.php';
			
			$data = array(
				'name' => $_POST['name'],
				'email' => $_POST['lastname'],
				'phone' => $_POST['phone'],
				'message' => $_POST['message'],
				'time' => date('d-m-Y H:i:s')
			);
			
			$result = senMail($data);
		}
		else
		{
			sleep(1);
			$result = true;
		}
		
		if ($bAjax)
		{
			echo json_encode(array(
				'result' => $result === true ? true : false,
				'error' => $result === true ? false : $result
			));
		}
		else
		{
			$sUrl = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
			header('Location: '.$sUrl, true, 302);
		}
			
		exit;
	}
	
	// $token = getToken();
	// session_start();
	// $_SESSION["token"] = $token;

	$route = "";
	
	if (isset($_SERVER['REQUEST_URI'])) {
		$oRequest = parse_url($_SERVER['REQUEST_URI']);
		$route = str_replace('/', '', $oRequest['path']);
    }
	
	switch ($route) {
		case "privacy-policy":
			include($route.'.php');
			break;
		case "" :
			include('main.php');
			break;               
		default:
            header('HTTP/1.1 404 Not Found');
            include('error.php');
			die();
	}