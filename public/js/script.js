$(document).ready(function() {
  var oOwlCarouselConfig = {
    transitionStyle: 'fade',
    items: 1,
    singleItem: true,
    navigation: true,
    navigationText: ['', 'See more details &rarr;'],
    pagination: true,
    autoHeight: true
  };

  if (window.matchMedia('all and (min-width: 1025px)').matches) {
    oOwlCarouselConfig.pagination = true;
    oOwlCarouselConfig.autoHeight = false;
  }
  var owl = $('.slider');
  owl.owlCarousel(oOwlCarouselConfig);

  $('.cases__item').on('mousewheel', function(e) {
    var curr = $(this)
      .closest('.cases__item')
      .find('.slider');

    var deltaY = Math.abs(e.deltaY);

    if (e.deltaX >= 1 && deltaY === 0) {
      _.debounce(curr.trigger('owl.next'), 20);
    } else if (e.deltaX <= -1 && deltaY === 0) {
      _.debounce(curr.trigger('owl.prev'), 20);
    }
  });

  $('.cases__nextBtn').click(function() {
    $(this)
      .closest('.slider')
      .trigger('owl.next');
  });

  window.app = {
    screenHeight: $(window).height(),
    sTextAppearanceDistance: '-50px'
  };

  if (window.matchMedia('all and (min-width: 1025px)').matches) {
    //		$('section').css("height", window.app.screenHeight);
    $('section.what_we_do, section.how_we_work, section.our_clients, section.contacts').css(
      'min-height',
      window.app.screenHeight
    );
    $('section.splash, section.xura, section.medicity, section.citybooq, section.netvision').css(
      'height',
      window.app.screenHeight
    );

    $(window).resize(function() {
      window.app.screenHeight = $(window).height();
      $('section.what_we_do, section.how_we_work, section.our_clients, section.contacts').css(
        'min-height',
        window.app.screenHeight
      );
      $('section.splash, section.xura, section.medicity, section.citybooq, section.netvision').css(
        'height',
        window.app.screenHeight
      );
    });
  } else {
    $('section').css('min-height', window.app.screenHeight);

    $(window).resize(function() {
      window.app.screenHeight = $(window).height();
      $('section').css('min-height', window.app.screenHeight);
    });
  }

  $('.cases__item').each(function() {
    new ScrollMagic.Scene({
      duration: 0,
      triggerElement: this,
      triggerHook: 0.3
    })
      .setTween(
        new TimelineMax({
          tweens: [
            TweenMax.from($(this).find('.cases__logo'), 0.8, {
              opacity: 0,
              y: 50,
              ease: Power2.easeOut
            }),
            TweenMax.from($(this).find('.cases__slide'), 0.8, {
              opacity: 0,
              y: 50,
              ease: Power2.easeOut
            })
          ]
        })
      )
      .addTo(window.SM.oMainController);

    new ScrollMagic.Scene({
      duration: 0,
      triggerElement: this,
      triggerHook: 0.35
    })
      .setTween(
        new TimelineMax({
          tweens: [
            TweenMax.from($(this).children('.cases__img'), 0.8, {
              opacity: 0,
              bottom: 450,
              ease: Power2.easeOut
            })
          ]
        })
      )
      .addTo(window.SM.oMainController);
  });
  $('.tech').each(function() {
    new ScrollMagic.Scene({
      duration: 0,
      triggerElement: this,
      triggerHook: 0.35
    })
      .setTween(
        new TimelineMax({
          tweens: [
            TweenMax.from($(this).find('.tech__item'), 0.8, {
              opacity: 0,
              transform: 'translateY(100px)',
              ease: Power2.easeOut
            })
          ]
        })
      )
      //.setClassToggle('.menu__btn', 'black')
      .addTo(window.SM.oMainController);
  });
});

window.SM = {
  oMainController: new ScrollMagic.Controller({
    //		addIndicators: true
  })
};

$(document).ready(function() {
  console.log('start animation');
  $('section:not(.splash)').addClass('hiddenSection');

  var oScene = null,
    iDuration = 1,
    oAutoAnimTimeline = new TimelineMax({
      repeat: 0,
      delay: 1.4,
      tweens: [
        TweenMax.from('#cover', iDuration, { height: '40%', ease: Power3.easeOut }),
        TweenMax.from('#el1', iDuration, { bottom: '40%', ease: Power3.easeOut }),
        TweenMax.from('#el3', iDuration, { bottom: '40%', ease: Power3.easeOut }),

        TweenMax.from('#el2', iDuration, { bottom: '20%', ease: Power3.easeOut }),
        TweenMax.from('#el4', iDuration, { bottom: '20%', ease: Power3.easeOut }),

        TweenMax.from('#el5', iDuration, { bottom: '15%', ease: Power3.easeOut }),
        TweenMax.from('#el6', iDuration, { bottom: '15%', ease: Power3.easeOut }),
        TweenMax.from('#mask', iDuration, { opacity: 1, ease: Power3.easeOut })
      ],
      onComplete: function() {
        oAutoAnimTimeline.clear();
        $('#mask').css({ opacity: 0 });
        $('section:not(.splash)').removeClass('hiddenSection');

        var oElem = $('section.splash .wrapper');
        oElem.css({ top: oElem.offset()['top'], left: 0, position: 'fixed' });

        if (!oScene) {
          var oScrollAnimTimeline = new TimelineMax();

          oScrollAnimTimeline
            .add(TweenMax.to('section.splash .mouse', 0.3, { opacity: 0, ease: Linear.easeNone }))
            .add(
              TweenMax.to('section.splash .description', 0.3, { opacity: 0, ease: Linear.easeNone })
            )
            .add(TweenMax.to('section.splash .title', 0.3, { opacity: 0, ease: Linear.easeNone }))
            .add(TweenMax.to('section.splash .navbar', 0.3, { opacity: 0, ease: Linear.easeNone }))
            .add(
              TweenMax.to('section.splash .main_logo', 0.3, { opacity: 0, ease: Linear.easeNone })
            );

          oScene = new ScrollMagic.Scene({
            duration: 300,
            offset: 0
          })
            .setTween(oScrollAnimTimeline)
            //						.triggerHook(0)
            //						.setPin("section.splash") // pins the element for the the scene's duration
            .addTo(window.SM.oMainController); // assign the scene to the controller

          new ScrollMagic.Scene({
            duration: 700,
            offset: 0
          })
            .setTween(
              new TimelineMax({
                tweens: [
                  TweenMax.to('#cover', 2, { height: '10%', ease: Linear.easeNone }),
                  TweenMax.to('#el1', 2, { bottom: '10%', ease: Linear.easeNone }),
                  TweenMax.to('#el3', 2, { bottom: '10%', ease: Linear.easeNone }),

                  TweenMax.to('#el2', 2, { bottom: '-30%', ease: Linear.easeNone }),
                  TweenMax.to('#el4', 2, { bottom: '-30%', ease: Linear.easeNone }),

                  TweenMax.to('#el5', 2, { bottom: '-70%', ease: Linear.easeNone }),
                  TweenMax.to('#el6', 2, { bottom: '-70%', ease: Linear.easeNone }),

                  TweenMax.to('#mask', 2, { opacity: 1, ease: Linear.easeNone })

                  //								TweenMax.to("section.splash .wrapper", 0.7, { opacity: 0, ease: Linear.easeNone})
                ],
                align: 'start'
              })
            )
            //						.triggerHook(0)
            //						.setPin("section.splash") // pins the element for the the scene's duration
            .addTo(window.SM.oMainController); // assign the scene to the controller
        }

        $('.slider')
          .data('owlCarousel')
          .reinit();
        // $('.what_we_do__slider').data('owlCarousel').reinit();
      }
    });
  new TweenMax.fromTo(
    '.splash .mouse div',
    1,
    { opacity: 0 },
    { repeat: -1, delay: 3, opacity: 1, yoyo: true, ease: Linear.easeNone }
  );
  new TweenMax.from('.splash .title .cursor', 0.5, {
    opacity: 0,
    repeat: -1,
    ease: Cubic.easeInOut
  });
  new TweenMax.to('.splash .title .text', 1, {
    text: { value: 'We do code', delimiter: '' },
    delay: 0.5,
    ease: Linear.easeNone
  });
  new TweenMax.to('.splash .errortitle .text', 1, {
    text: { value: '404', delimiter: '' },
    delay: 0.5,
    ease: Linear.easeNone
  });
  new TweenMax.from('.splash .description', 0.7, { opacity: 0, delay: 2, ease: Cubic.easeOut });
  new TweenMax.from('.splash .main_logo', 1, { opacity: 0, delay: 2.2, ease: Cubic.easeOut });
  new TweenMax.from('.splash .navbar', 1, { opacity: 0, delay: 2.4, ease: Cubic.easeOut });
});

$(document).ready(function() {
  //how_we_work slide animation
  var aTweens = null,
    iTriggerHook = 0.5;
  if (window.matchMedia('all and (min-width: 1280px)').matches) {
    aTweens = [
      TweenMax.from('.how_we_work .stage', 1, { opacity: 0, ease: Power2.easeOut }),
      TweenMax.from('.how_we_work .stage1', 1, { left: '-5%', ease: Power2.easeOut }),
      TweenMax.from('.how_we_work .stage2', 1, { left: '-20%', ease: Power2.easeOut }),
      TweenMax.from('.how_we_work .stage3', 1, { left: '-35%', ease: Power2.easeOut }),
      TweenMax.from('.how_we_work .stage4', 1, { left: '-50%', ease: Power2.easeOut }),
      TweenMax.from('.how_we_work .stage5', 1, { left: '-65%', ease: Power2.easeOut }),
      TweenMax.from('.how_we_work .stage6', 1, { left: '-80%', ease: Power2.easeOut })
    ];
  } else {
    iTriggerHook = 0.3;

    aTweens = [
      TweenMax.from('.how_we_work .stage1', 1, { top: '-50px', ease: Power2.easeOut }),
      TweenMax.from('.how_we_work .stage2', 1, { top: '-220px', ease: Power2.easeOut }),
      TweenMax.from('.how_we_work .stage3', 1, { top: '-390px', ease: Power2.easeOut }),
      TweenMax.from('.how_we_work .stage4', 1, { top: '-560px', ease: Power2.easeOut }),
      TweenMax.from('.how_we_work .stage5', 1, { top: '-730px', ease: Power2.easeOut }),
      TweenMax.from('.how_we_work .stage6', 1, { top: '-900px', ease: Power2.easeOut })
    ];
  }

  new ScrollMagic.Scene({
    duration: 0,
    triggerElement: 'section.how_we_work',
    triggerHook: iTriggerHook
  })
    .setTween(
      new TimelineMax({
        tweens: aTweens,
        align: 'start'
      })
    )
    .addTo(window.SM.oMainController);

  //animation of texts
  new ScrollMagic.Scene({
    duration: 0,
    triggerElement: 'section.how_we_work',
    triggerHook: 0.6
  })
    .setTween(
      new TimelineMax({
        tweens: [
          TweenMax.staggerFrom(
            'section.how_we_work h2, section.how_we_work .description',
            1,
            { opacity: 0, bottom: window.app.sTextAppearanceDistance, ease: Power2.easeOut },
            0.5
          )
        ],
        align: 'start'
      })
    )
    .addTo(window.SM.oMainController);
});

$(document).ready(function() {
  //	var
  //		oControllerV = new ScrollMagic.Controller({
  //			addIndicators: true
  //		})
  //	;
  //
  //	new ScrollMagic.Scene({
  //			duration: 0,
  //			triggerElement: "section.xura"
  //		})
  //		.triggerHook(1)
  //		.addTo(oControllerV)
  //		.on("enter", function (event) {
  //			$('section.xura video')[0].play();
  //		})
  //		.on("leave", function (event) {
  //			$('section.xura video')[0].pause();
  //		})
  //	;

  ///// xura
  new ScrollMagic.Scene({
    duration: 0,
    triggerElement: 'section.xura',
    triggerHook: 0.5
  })
    .setTween(TweenMax.from('section.xura video', 2, { opacity: 0, ease: Power2.easeOut }))
    //		.addIndicators({name: "video xura"})
    .addTo(window.SM.oMainController);

  ///// MEDICITY
  new ScrollMagic.Scene({
    duration: 0,
    triggerElement: 'section.medicity',
    triggerHook: 0.5
  })
    .setTween(
      new TimelineMax({
        tweens: [
          TweenMax.from('section.medicity video', 2, { opacity: 0, ease: Power2.easeOut }),
          TweenMax.to('section.xura video', 2, { opacity: 0, ease: Power2.easeOut })
        ],
        align: 'start'
      })
    )
    //		.addIndicators({name: "video medicity"})
    .addTo(window.SM.oMainController);

  ///// citybooq
  new ScrollMagic.Scene({
    duration: 0,
    triggerElement: 'section.citybooq',
    triggerHook: 0.5
  })
    .setTween(
      new TimelineMax({
        tweens: [
          TweenMax.from('section.citybooq video', 2, { opacity: 0, ease: Power2.easeOut }),
          TweenMax.to('section.medicity video', 2, { opacity: 0, ease: Power2.easeOut })
        ],
        align: 'start'
      })
    )
    //		.addIndicators({name: "video citybooq"})
    .addTo(window.SM.oMainController);

  ///// netvision
  new ScrollMagic.Scene({
    duration: 0,
    triggerElement: 'section.netvision',
    triggerHook: 0.5
  })
    .setTween(
      new TimelineMax({
        tweens: [
          TweenMax.from('section.netvision video', 2, { opacity: 0, ease: Power2.easeOut }),
          TweenMax.to('section.citybooq video', 2, { opacity: 0, ease: Power2.easeOut })
        ],
        align: 'start'
      })
    )
    //		.addIndicators({name: "video netvision"})
    .addTo(window.SM.oMainController);
});

$(document).ready(function() {
  var className = 'what_we_do__tabs-item';
  var activeClassName = 'what_we_do__tabs-item--active';

  pureTabs.init(className, activeClassName);

  new ScrollMagic.Scene({
    triggerElement: 'section.what_we_do',
    duration: 0
  })
    .setTween(
      new TimelineMax({
        tweens: [
          TweenMax.to('#menuBtn', 0.5, {
            display: 'block',
            opacity: '1',
            ease: Linear.easeNone
          })
        ],
        align: 'start'
      })
    )
    .setPin('#menuBtn')
    .addTo(window.SM.oMainController);

  new ScrollMagic.Scene({
    triggerElement: '.what_we_do__tabs',
    offset: 120,
    duration: 0,
    triggerHook: 0.6,
    reverse: false
  })
    .setTween(
      new TimelineMax({
        tweens: [
          TweenMax.from('.what_we_do__tabs-item', 1.2, {
            opacity: 0,
            bottom: window.app.sTextAppearanceDistance,
            ease: Power2.easeOut
          }),
          TweenMax.to('.what_we_do__tabs-item', 1.5, {
            borderColor: '#68c6ff',
            ease: Power2.easeOut
          }),
          TweenMax.from('.what_we_do__tabs-item--active', 1.5, {
            borderColor: 'transparent',
            background: 'transparent',
            color: '#fff',
            ease: Linear.easeNone
          }),
          TweenMax.from('.what_we_do__tabs-item--active path', 1.8, {
            fill: '#fff',
            ease: Linear.easeNone
          }),
          TweenMax.from('.what_we_do__tabs-item--active', 0.8, {
            color: '#fff',
            ease: Linear.easeNone
          })
        ],
        onComplete: function() {
          $('.what_we_do__tabs-item--active .what_we_do__tabs-icon path[style]').removeAttr(
            'style'
          );
          $('.what_we_do__tabs-item--active[style]').attr('style', 'border-color: #68c6ff');
        },
        align: 'start'
      })
    )
    .addTo(window.SM.oMainController);

  new ScrollMagic.Scene({
    triggerElement: '.what_we_do__tabs',
    offset: 260,
    duration: 00,
    triggerHook: 0.6,
    reverse: false
  })
    .setTween(
      new TimelineMax({
        tweens: [
          TweenMax.to('.what_we_do__descr-text', 2.5, {
            borderColor: '#68c6ff',
            delay: 5.5,
            ease: Linear.easeNone
          }),
          TweenMax.from('.what_we_do__full-descrWrapper', 1.8, {
            opacity: '0',
            bottom: window.app.sTextAppearanceDistance,
            delay: 4.5,
            ease: Power2.easeOut
          })
        ],
        align: 'start'
      })
    )
    .addTo(window.SM.oMainController);

  $('.what_we_do__slider').owlCarousel({
    autoHeight: true,
    items: 1,
    singleItem: true
  });

  function setPanelHeight() {
    var max_col_height = 0;
    $('.what_we_do__full-descr').each(function() {
      if ($(this).height() > max_col_height) {
        max_col_height = $(this).height();
      }
    });

    $('.what_we_do__descr-text').height(max_col_height + 120);
  }

  setPanelHeight();

  $(window).resize(function() {
    setPanelHeight();
  });
});
$(document).ready(function() {
  $('.cases__item').each(function() {
    var element = this;
    new ScrollMagic.Scene({
      duration: 0,
      triggerElement: element,
      duration: 0,
      triggerHook: 0
    })
      .on('enter', function() {
        $(element)
          .find('.owl-next')
          .addClass('active');
      })
      .on('leave', function() {
        $(element)
          .find('.owl-next')
          .removeClass('active');
      })
      .addTo(window.SM.oMainController);
  });
});

$(document).ready(function() {
  new ScrollMagic.Scene({
    duration: 0,
    triggerElement: 'section.our_clients',
    triggerHook: 0.7
  })
    .setTween(
      TweenMax.from('section.our_clients h2', 1, {
        opacity: 0,
        bottom: window.app.sTextAppearanceDistance,
        ease: Power2.easeOut
      })
    )
    //	.addIndicators({name: "video netvision"})
    .addTo(window.SM.oMainController);

  var sSelector = '.our_clients .container-fluid > div';
  if (!window.matchMedia('all and (min-width: 1280px)').matches) {
    sSelector = '.our_clients .container-fluid .logotype';
  }

  $(sSelector).each(function(iIndex, oItem) {
    new ScrollMagic.Scene({
      duration: 0,
      triggerElement: oItem,
      triggerHook: 0.7
    })
      .setTween(TweenMax.from(oItem, 1, { opacity: 0, bottom: '-50px', ease: Power2.easeOut }))
      //	.addIndicators({name: "video netvision"})
      .addTo(window.SM.oMainController);
  });

  new ScrollMagic.Scene({
    duration: window.app.screenHeight / 2,
    triggerElement: 'section.contacts',
    triggerHook: 1
  })
    .setTween(
      TweenMax.to('section.our_clients, section.contacts', 1.5, { backgroundColor: 'white' })
    )
    .addTo(window.SM.oMainController);
});

$(document).ready(function() {
  new ScrollMagic.Scene({
    duration: 0,
    triggerElement: 'section.contacts',
    triggerHook: 0.4
  })
    .setTween(
      new TimelineMax({
        tweens: [
          TweenMax.from('.contacts .address', 2, { opacity: 0, ease: Power2.easeOut }),
          TweenMax.from('.contacts .form', 2, { opacity: 0, ease: Power2.easeOut })
        ]
        // align: 'start'
      })
    )
    // .addIndicators({name: ".contacts"})
    .addTo(window.SM.oMainController);

  new ScrollMagic.Scene({
    duration: 0,
    triggerElement: 'section.contacts',
    triggerHook: 0.2
  })
    .setTween(TweenMax.from('section.contacts .main_logo', 2, { opacity: 0, delay: 1 }))
    // .addIndicators({name: ".contacts"})
    .addTo(window.SM.oMainController);

  $.formUtils.addValidator({
    name: 'email',
    validatorFunction: function(value, $el, config, language, $form) {
      return true;
    },
    errorMessage: '',
    errorMessageKey: 'badEvenNumber'
  });
  $.formUtils.addValidator({
    name: 'lastname',
    validatorFunction: function(sText) {
      var emailParts = sText.toLowerCase().split('@'),
        localPart = emailParts[0],
        domain = emailParts[1];
      if (localPart && domain) {
        if (localPart.indexOf('"') === 0) {
          var len = localPart.length;
          localPart = localPart.replace(/\"/g, '');
          if (localPart.length !== len - 2) {
            return false; // It was not allowed to have more than two apostrophes
          }
        }

        return (
          $.formUtils.validators.validate_domain.validatorFunction(emailParts[1]) &&
          localPart.indexOf('.') !== 0 &&
          localPart.substring(localPart.length - 1, localPart.length) !== '.' &&
          localPart.indexOf('..') === -1 &&
          !/[^\w\+\.\-\#\-\_\~\!\$\&\'\(\)\*\+\,\;\=\:]/.test(localPart)
        );
      }

      return false;
    },
    errorMessage: 'Please provide a correct email',
    errorMessageKey: 'badEmail'
  });

  $.validate({
    form: '#contact_form',
    validateOnBlur: true,
    inlineErrorMessageCallback: function($input, errorMessage, config) {
      // console.log($input);
      // return true;
      // Return an element that should contain the error message.
      // This callback will called when validateOnBlur is set to true (default) and/or errorMessagePosition is set to 'inline'
    },
    submitErrorMessageCallback: function($form, errorMessages, config) {
      // Return an element that should contain all error messages.
      // This callback will be called when errorMessagePosition is set to 'top'
    }
    // onError : function($form) {
    // alert('Validation of form '+$form.attr('id')+' failed!');
    // return true;
    // },
    // onSuccess : function($form) {
    // alert('The form '+$form.attr('id')+' is valid!');
    // return false; // Will stop the submission of the form
    // }
  });

  var options = {
      hashTracking: false
    },
    oPopup = $('[data-remodal-id=modal]').remodal(options),
    oPopupCont = $('.remodal'),
    oButton = oPopupCont.find('.btn'),
    oText = oPopupCont.find('.text');
  $('section.contacts form').bind('submit', function(event) {
    event.preventDefault();
    var sData = $(this).serialize() + '&ajax=1';

    $.post(
      '/',
      sData,
      function(data) {
        if (data.result) {
          oButton.removeClass('btn-warning').addClass('btn-success');
          oText.html(
            'Thank you!<br /> The message was sent successfully.<br /> We will contact you as soon as possible.'
          );
        } else {
          oButton.removeClass('btn-success').addClass('btn-warning');
          oText.html('Oops, some problem has occurred.<br /> Please try again.');
          console.log('Error: ' + data.error);
        }

        oPopup.open();
      },
      'json'
    );

    return false;
  });
});

var menu = document.querySelector('.menu');
var menuBtn = document.querySelector('.menu__btn');
var overlay = document.querySelector('.menu__overlay');

menuBtn.addEventListener('click', menuToggle);

document.addEventListener('click', closeMenu);

function closeMenu(e) {
  if (!e.target.closest('.menu') && !Object.is(e.target, menuBtn)) {
    menu.classList.remove('menu_opened');
    menuBtn.classList.remove('active');
    overlay.classList.remove('active');
  }
}

function closeAfterScroll() {
  menu.classList.remove('menu_opened');
  menuBtn.classList.remove('active');
  overlay.classList.remove('active');
}

function menuToggle() {
  var menuScene = new ScrollMagic.Scene({
    triggerElement: 'section.what_we_do',
    duration: 0
  }).setClassToggle('.menu', 'menu_opened');

  menuScene.on('leave', function(event) {
    menuScene.destroy(true);
    menuBtn.classList.remove('active');
    overlay.classList.remove('active');
  });

  var classStatus = menu.classList.contains('menu_opened');
  if (classStatus) {
    menu.classList.remove('menu_opened');
    menuBtn.classList.remove('active');
    overlay.classList.remove('active');
    window.SM.oMainController.removeScene(menuScene);
  } else {
    menu.classList.add('menu_opened');
    menuBtn.classList.add('active');
    overlay.classList.add('active');
    window.SM.oMainController.addScene(menuScene);
  }
}

var spy = new Gumshoe('#menu a');
var scroll = new SmoothScroll('a[href*="#"]', {
  speed: 250
});

$(document).ready(function() {
  new ScrollMagic.Scene({
    offset: 0,
    triggerElement: 'section.how_we_work',
    triggerHook: 0.3
  })
    .setClassToggle('.menu__btn', 'black')
    .addTo(window.SM.oMainController);
});

document.addEventListener('scrollStop', closeAfterScroll);

$(document).ready(function() {
  var specHeight;
  setTimeout(function() {
    specHeight = $('.our-specializations').height();

    new ScrollMagic.Scene({
      triggerElement: 'section.our-specializations',
      triggerHook: 0.1,
      duration: specHeight
    })
      .setClassToggle('.menu__btn', 'black')
      .addTo(window.SM.oMainController);
  }, 3700);

  $('.our-specializations__item').each(function() {
    new ScrollMagic.Scene({
      duration: 0,
      triggerElement: this,
      triggerHook: 0.8
    })
      .setTween(
        new TimelineMax({
          tweens: [
            TweenMax.from($(this), 0.8, {
              opacity: 0,
              y: 50,
              ease: Power2.easeOut
            })
          ]
        })
      )
      .addTo(window.SM.oMainController);
  });
});

function scrollToAnchor() {
  if (window.location.hash) {
    console.log($(window.location.hash).offset().top);
    $('html, body').animate(
      {
        scrollTop: $(window.location.hash).offset().top
      },
      1000,
      'swing'
    );
  }
}
$(window).load(function() {
  scrollToAnchor();
});
//setTimeout(scrollToAnchor, 2500);
