var gulp = require('gulp'),
  // imagemin = require('gulp-imagemin'),
  concat = require('gulp-concat'),
  sass = require('gulp-sass'),
  rev = require('gulp-rev');

/*
 * Создаём задачи
 *
 * concat – для склейки всех CSS и JS в отдельные файлы
 */

gulp.task('sass', function() {
  return gulp
    .src(['./src/sass/_bootstrap.scss', './src/sass/main.scss'])
    .pipe(concat('main.scss'))
    .pipe(sass().on('error', sass.logError))
    .pipe(rev())
    .pipe(gulp.dest('./public/css/'))
    .pipe(
      rev.manifest({
        merge: true
      })
    )
    .pipe(gulp.dest('public'));
});

gulp.task('js:libs', function() {
  return gulp
    .src([
      './src/js/vendor/jquery-1.11.3.min.js',
      './src/js/vendor/underscore-min.js',
      './src/js/vendor/media-match-master/media.match.min.js',
      './src/js/vendor/owl.carousel.min.js',
      './src/js/vendor/jquery.mousewheel.js',
      './src/js/vendor/ScrollMagic.min.js',
      './src/js/vendor/animation.gsap.min.js',
      './src/js/vendor/debug.addIndicators.min.js',
      './src/js/vendor/gsap/TweenMax.min.js',
      './src/js/vendor/gsap/TimelineMax.min.js',
      './src/js/vendor/gsap/plugins/TextPlugin.min.js',
      './src/js/vendor/jquery.form-validator.min.js',
      './src/js/vendor/remodal.min.js',
      './src/js/vendor/puretabs.js',
      './src/js/vendor/gumshoe.min.js',
      './src/js/vendor/smooth-scroll.min.js'
    ])
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('./public/js/'));
});

gulp.task('js:script', function() {
  return gulp
    .src([
      './src/js/main.js',
      './src/js/splash.js',
      './src/js/how_we_work.js',
      './src/js/video.js',
      './src/js/what_we_do.js',
      './src/js/owl-next.js',
      './src/js/clients.js',
      './src/js/contacts.js',
      './src/js/menu.js',
      './src/js/specializations.js',
      './src/js/scrollToAnchor.js'
    ])
    .pipe(concat('script.js'))
    .pipe(gulp.dest('./public/js/'));
});

// gulp.task('concat', function(){

// gulp.src('./public/js/*.js')
// .pipe(concat('scripts.js'))
// .pipe(gulp.dest('./public/min/'))

// gulp.task('sass');

// gulp.src('./public/css/*.css')
// .pipe(concat('styles.css'))
// .pipe(gulp.dest('./public/min/'))
// });

// gulp.task('imagemin',function(){
// gulp.src('./img/**/*')
// .pipe(imagemin())
// .pipe(gulp.dest('./public/img/'));
// });

/*
 * Создадим задачу, смотрящую за изменениями
 */


gulp.task("watch", function() {
    // Watch .scss files
    gulp.watch("./src/sass/**/*.scss", gulp.parallel('sass'));
    // Watch .js files
    gulp.watch("./src/js/**/*.js", gulp.parallel(['js:libs', 'js:script']));
    console.log('\x1b[33m%s\x1b[0m', 'Server starts on http://localhost:9000');
});

gulp.task(
    "default",
    gulp.parallel(
        'sass',
        'js:libs',
        'js:script',       
        'watch'
    )
);
